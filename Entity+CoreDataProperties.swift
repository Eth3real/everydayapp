//
//  Entity+CoreDataProperties.swift
//  EverydayApp
//
//  Created by FrancescaCiancio on 01/06/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//
//

import Foundation
import CoreData


extension Entity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Entity> {
        return NSFetchRequest<Entity>(entityName: "Entity")
    }

    @NSManaged public var challenge: String?
    @NSManaged public var challengeType: String?
    @NSManaged public var day: Date?
    @NSManaged public var done: Bool
    @NSManaged public var notion: String?
    @NSManaged public var resultImage: Data?
    @NSManaged public var whenDone: Date?

}
