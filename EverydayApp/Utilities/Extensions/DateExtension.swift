//
//  DateExtension.swift
//  EverydayApp
//
//  Created by Simone Formisano on 04/06/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import Foundation

extension Date {
    static func getTodaysDay() -> Int {
        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day], from: date)
        return components.day!
    }
    
    static func getDay(date: Date) -> Int {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day], from: date)
        return components.day!
    }
    
    /// Extracts the month string from a given Date object
    static func extractMonth(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "LLLL"
        let nameOfMonth = dateFormatter.string(from: date)
        return nameOfMonth
    }
    
    static func getLastDayOfTheMonth() -> Int {
        let calendar = NSCalendar.current
        let dateFormatter = DateFormatter()
        let date = Date()
        
        dateFormatter.dateFormat = "dd"
        let components = calendar.dateComponents([.year, .month], from: date)
        let startOfMonth = calendar.date(from: components)!
        
        var comps2 = DateComponents()
        comps2.month = 1
        comps2.day = -1
        let endOfMonth = calendar.date(byAdding: comps2, to: startOfMonth)!
        
        return Int(dateFormatter.string(from: endOfMonth))!
    }
    
    static func getTodaysFormattedDate(formattedString: String) -> String {
          let now = Date()

          let formatter = DateFormatter()
          formatter.dateFormat = formattedString
          
          let datetime = formatter.string(from: now)
      
          return datetime
    }
    
    /// E.g. Thursday, 4 June 2020
    static func getComplateDate(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        
        let datetime = formatter.string(from: date)
    
        return datetime
    }
}
