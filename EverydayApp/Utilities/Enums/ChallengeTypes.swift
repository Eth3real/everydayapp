//
//  ChallengeType.swift
//  EverydayApp
//
//  Created by Simone Formisano on 21/05/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

// Enum for the challenge types
enum ChallengeTypes {
    static let audio = "audio"
    static let picture = "picture"
    static let drawing = "drawing"
}
