//
//  HeroIDsEnum.swift
//  EverydayApp
//
//  Created by Simone Formisano on 13/05/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import Foundation

// Hero IDs for syncing elements of differents VC animation during custom Hero transitions
enum HeroIDs {
    static let taskImageHeroID = "taskImageID"
    static let taskCardViewHeroID = "taskCardViewID"
    static let taskTitleHeroID = "taskTitleViewID"
    static let taskBackgroundImageID = "taskBackgroundImageID"
}
