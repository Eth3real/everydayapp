//
//  ImageSource.swift
//  EverydayApp
//
//  Created by Simone Formisano on 04/06/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import Foundation

enum ImageSource {
    case photoLibrary
    case camera
}
