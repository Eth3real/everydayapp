//
//  MyViews.swift
//  struttura
//
//  Created by FrancescaCiancio on 16/05/2020.
//  Copyright © 2020 Francesca Ciancio. All rights reserved.
//



import UIKit
enum myFont {
    case bold
    case regular
    case custom
}

class MyLabel : UILabel {
    var labelTitle : String!
    var lines : Int? // i can make optional this value
    var labelFont : myFont!
    var color : UIColor?
    var allignment : NSTextAlignment?
    convenience init  (labelTitle: String, lines: Int?=1, labelFont: myFont, color: UIColor? = .black, allignment:NSTextAlignment? = .left){
        self.init()
        
        switch labelFont {
        case .bold :  font = .boldSystemFont(ofSize: 15)//UIFont(name: "SF Compat Text Medium", size: 15)
        case .regular: font = .systemFont(ofSize: 15) //UIFont(name: "SF Compat Text Regular", size: 15)
        case .custom: font = UIFont(name: "SportingGrotesque-Regular", size: 15)
        }
        
        numberOfLines = lines!
        textColor = color
        text = labelTitle
        textAlignment = allignment!
    }
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

