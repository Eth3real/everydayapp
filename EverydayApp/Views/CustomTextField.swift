//
//  CustomTextField.swift
//  EverydayApp
//
//  Created by Giada Ciotola on 04/06/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import UIKit

@IBDesignable
    open class customUITextField: UITextField {

        func setup() {
            let border = CALayer()
            let width = CGFloat(2.0)
            border.borderColor = UIColor(red: 179/255, green: 172/255, blue: 250/255, alpha: 1).cgColor
            border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: self.frame.size.height)
            border.borderWidth = width
            self.layer.addSublayer(border)
            self.layer.masksToBounds = true
        
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
}
