//
//  CustomButton.swift
//  EverydayApp
//
//  Created by Simone Formisano on 16/05/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import UIKit

@IBDesignable
final class CustomButton: UIButton {
    var borderWidth: CGFloat = 1.5
    var borderColor = UIColor.black.cgColor

    @IBInspectable var titleText: String? {
        didSet {
            self.setTitle(titleText, for: .normal)
            self.setTitleColor(UIColor.black,for: .normal)
        }
    }

    override init(frame: CGRect){
        super.init(frame: frame)
    }
    
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }

    
    override func draw(_ rect: CGRect) {
        
       self.clipsToBounds = true
//          self.layer.cornerRadius = 10
          self.layer.borderColor = borderColor
          self.layer.borderWidth = borderWidth
        
        // Respond to touch events by user
        self.addTarget(self, action: #selector(onPress), for: .touchDown)
        self.addTarget(self, action: #selector(onRelase), for: .touchUpInside)
    }
    
    @objc func onPress() {
        setSelected()
    }
    
    @objc func onRelase() {
         setDeselected()
    }
       
       // Set the selected properties
       func setSelected() {
        self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.10)
       }
       
       // Set the deselcted properties
       func setDeselected() {
        self.backgroundColor = .none
       }
}
