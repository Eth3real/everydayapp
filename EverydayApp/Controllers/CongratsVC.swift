//
//  CongratsVC.swift
//  EverydayApp
//
//  Created by Simone Formisano on 17/05/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import UIKit
import CoreData

class CongratsVC: UIViewController {
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var congratsName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initCardView()
        congratsName.text = "Well done, \(UserDefaults.standard.string(forKey: "username")!)!"
    }

    private func initCardView() {
        cardView.layer.borderWidth = 2
        cardView.layer.borderColor = CGColor(srgbRed: 0, green: 0, blue: 0, alpha: 1.0)
    }
}
