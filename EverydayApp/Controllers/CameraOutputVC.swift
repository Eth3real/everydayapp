//
//  CameraOutputVC.swift
//  EverydayApp
//
//  Created by Simone Formisano on 17/05/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import UIKit

class CameraOutputVC: UIViewController, UINavigationControllerDelegate {
    @IBOutlet weak var imageTake: UIImageView!
    @IBOutlet weak var takePictureBtn: UIButton!
    @IBOutlet weak var photoFrameView: UIView!
    private var imagePicker: UIImagePickerController!
    var todaysChallenge: Entity!
    var comingFromHistory = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !comingFromHistory {
            todaysChallenge = CoreDataController().getTodaysChallenge()
        }
        
       if todaysChallenge.done {
           takePictureBtn.alpha = 0.0
           imageTake.image = UIImage(data: todaysChallenge.resultImage!)
           takePictureBtn.alpha = 0.0
        UIView.animate(withDuration: 1.5, animations: {
            self.photoFrameView.alpha = 0.0
            self.imageTake.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        })
        imageTake.heroID = HeroIDs.taskImageHeroID
       }
        
        photoFrameView.layer.shadowColor = UIColor.black.cgColor
        photoFrameView.layer.shadowOpacity = 0.30
        photoFrameView.layer.shadowOffset = .zero
        photoFrameView.layer.shadowRadius = 7
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !comingFromHistory {
            todaysChallenge = CoreDataController().getTodaysChallenge()
        }
        
        if todaysChallenge.done {
            takePictureBtn.alpha = 0.0
            imageTake.image = UIImage(data: todaysChallenge.resultImage!)
            takePictureBtn.alpha = 0.0
             UIView.animate(withDuration: 1.5, animations: {
                self.photoFrameView.alpha = 0.0
                self.imageTake.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
                  })
        }
        
        imageTake.heroID = HeroIDs.taskImageHeroID
    }
    
    //MARK: - Take image
    
    @IBAction func takePhoto(_ sender: Any) {
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
            selectImageFrom(.photoLibrary)
            return
        }
        selectImageFrom(.camera)
    }

   private func selectImageFrom(_ source: ImageSource){
       imagePicker =  UIImagePickerController()
       imagePicker.delegate = self
       switch source {
       case .camera:
           imagePicker.sourceType = .camera
       case .photoLibrary:
           imagePicker.sourceType = .photoLibrary
       }
       present(imagePicker, animated: true, completion: nil)
   }

   //MARK: - Saving Image here
   @IBAction func save(_ sender: AnyObject) {
       guard let selectedImage = imageTake.image else {
           print("Image not found!")
           return
       }
       UIImageWriteToSavedPhotosAlbum(selectedImage, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
   }

   //MARK: - Add image to Library
    
   @objc private func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
       if let error = error {
           // we got back an error!
           showAlertWith(title: "Save error", message: error.localizedDescription)
       } else {
           showAlertWith(title: "Saved!", message: "Your image has been saved to your photos.")
       }
   }

   private func showAlertWith(title: String, message: String){
       let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
       ac.addAction(UIAlertAction(title: "OK", style: .default))
       present(ac, animated: true)
   }
}

extension CameraOutputVC: UIImagePickerControllerDelegate {
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        imagePicker.dismiss(animated: true, completion: nil)
        guard let selectedImage = info[.originalImage] as? UIImage else {
            print("Image not found!")
            return
        }
        imageTake.image = selectedImage
        todaysChallenge.setValue(true, forKey: "done")
        todaysChallenge.setValue(imageTake.image?.jpegData(compressionQuality: 0.8), forKey: "resultImage")
        todaysChallenge.setValue(Date(), forKey: "whenDone")
        try? todaysChallenge.managedObjectContext?.save()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.7) {
            let parent = self.parent as? ExpandedTaskVC
            parent?.executeSegueToCongrats()
        }
    }
}

