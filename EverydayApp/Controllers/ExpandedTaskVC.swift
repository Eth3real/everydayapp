//
//  ExpandedTaskVC.swift
//  HeroTransition_test-cardView
//
//  Created by Simone Formisano on 12/05/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import UIKit
import Hero

class ExpandedTaskVC: UIViewController, UINavigationControllerDelegate {
    
    /// Container views for the different challenge outputs
    @IBOutlet weak var containerViewOutputDrawing: UIView!
    @IBOutlet weak var containerViewOutputAudio: UIView!
    @IBOutlet weak var containerViewOutputPicture: UIView!
    @IBOutlet weak var challengeHint: UILabel!
    @IBOutlet weak var challengeDescription: UILabel!
    @IBOutlet weak var todaysDate: UILabel!
    @IBOutlet weak var headerBackgroundView: UIView!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var shareLbl: UILabel!
    @IBOutlet weak var disposeView: UIView!
    
    /// Tells if the segue happened from the history so that will not access todays challenge, because it will recive the challenge from that
    var comingFromHistory = false
    var todaysChallenge: Entity!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        inizialize()
        
        if todaysChallenge.done {
            challengeHint.alpha = 0.0
            UIView.animate(withDuration: 1.5, animations: {
                  self.headerBackgroundView.alpha = 1.0
              })
            shareBtn.isHidden = false
            shareLbl.isHidden = false
            todaysDate.text = Date.getComplateDate(date: todaysChallenge.whenDone!)
        } else {
            shareBtn.isHidden = true
            shareLbl.isHidden = true
        }
        shareBtn.addTarget(self, action: #selector(shareAction), for: .touchUpInside)
    }
    
    /// --- Begin Inner classes UIActivityItemProvider for sharing functionlities ---
    private class ChallengeTextItemProvider: UIActivityItemProvider {
        var challenge: String?
        
        init(challenge: String) {
            super.init(placeholderItem: "")
            self.challenge = challenge
        }
        
        override var item: Any {
            if (activityType?.rawValue.lowercased().contains("whatsapp"))! {
                return "" // WhatsApp doesn't allow image + text sending
            } else {
                return challenge!
            }
        }
    }

    private class ChallengeHashtagItemProvider: UIActivityItemProvider {
        var hashtag: String?
        
        init(hashtag: String) {
            super.init(placeholderItem: "")
            self.hashtag = hashtag
        }
        
        override var item: Any {
            if (activityType?.rawValue.lowercased().contains("whatsapp"))! {
                return "" // WhatsApp doesn't allow image + text sending
            } else {
                return hashtag!
            }
        }
    }
    /// --- End Inner classes UIActivityItemProvider for sharing functionlities ---
        
    @objc private func shareAction() {
        let challengeDescription = todaysChallenge.challenge
        let hashtag = "#SternaChallenge"
        let challengeResultImage = UIImage(data: todaysChallenge.resultImage!)
        let logo = UIImage(named: "logo")! // 710x500 (original size)
        let resultImageHeight = 1024
        let resultImageWidth = 768
        let resultImageSize = CGSize(width: resultImageWidth, height: resultImageHeight)
        let logoHeight = 140
        let logoWidth = 205
        
        /// Begin Sterna logo adding
        UIGraphicsBeginImageContextWithOptions(resultImageSize, false, 0.0)
        challengeResultImage!.draw(in: CGRect(origin: CGPoint.zero, size: resultImageSize))
        logo.draw(in: CGRect(origin: CGPoint(x: 20, y: 1024 - 120 - 20), size: CGSize(width: logoWidth, height: logoHeight)))
        let imageWithLogo = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        /// End Sterna logo adding

        let challengeText = ChallengeTextItemProvider(challenge: challengeDescription!)
        let challengeHashtag = ChallengeHashtagItemProvider(hashtag: hashtag)
        
        let activityViewController = UIActivityViewController(activityItems: [imageWithLogo, challengeText, challengeHashtag], applicationActivities: nil)
        
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if todaysChallenge.done {
            challengeHint.alpha = 0.0
            UIView.animate(withDuration: 1.5, animations: {
                self.headerBackgroundView.alpha = 1.0
            })
            shareBtn.isHidden = false
            shareLbl.isHidden = false
            todaysDate.text = Date.getComplateDate(date: todaysChallenge.whenDone!)

        } else {
            shareBtn.isHidden = true
            shareLbl.isHidden = true
        }
        
         headerBackgroundView.layer.cornerRadius = headerBackgroundView.bounds.width / 2
    }

    private func inizialize() {
        changeContainerViewBasedOnOutput(todaysChallenge)
        /// Setting pan gesture for dismissing the view
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panAction))
        disposeView.isUserInteractionEnabled = true
        disposeView.addGestureRecognizer(panGesture)
        disposeView.addGestureRecognizer(panGesture)
        setHeroIDs()
        changeLabels()
        
        
        headerBackgroundView.alpha = 0.0
        shareBtn.isHidden = true
        shareLbl.isHidden = true
    }

    func executeSegueToCongrats() {
        performSegue(withIdentifier: "goCongrats", sender: self)
    }

    /// Passing the comingFromHistory variable
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "goPicture":
            let destinationVC = segue.destination as! CameraOutputVC
            destinationVC.comingFromHistory = comingFromHistory
            destinationVC.todaysChallenge = todaysChallenge
        case "goDrawing":
            let destinationVC = segue.destination as! DrawingVC
            destinationVC.comingFromHistory = comingFromHistory
            destinationVC.todaysChallenge = todaysChallenge
        default: break;
        }
    }
    
    private func changeLabels() {
        challengeDescription.text = todaysChallenge.challenge
        challengeHint.text = todaysChallenge.notion
    }
    
    private func changeContainerViewBasedOnOutput(_ selectedChallenge: Entity) {
        switch selectedChallenge.challengeType {
        case ChallengeTypes.audio:
            containerViewOutputDrawing.alpha = 0
            containerViewOutputAudio.alpha = 1
            containerViewOutputPicture.alpha = 0
        case ChallengeTypes.drawing:
            containerViewOutputDrawing.alpha = 1
            containerViewOutputAudio.alpha = 0
            containerViewOutputPicture.alpha = 0
        case ChallengeTypes.picture:
            containerViewOutputDrawing.alpha = 0
            containerViewOutputAudio.alpha = 0
            containerViewOutputPicture.alpha = 1
        default:
            break
        }
    }
    
    private func setHeroIDs() {
        view.heroID = HeroIDs.taskCardViewHeroID
        headerBackgroundView.heroID = HeroIDs.taskBackgroundImageID
        todaysDate.heroID = HeroIDs.taskTitleHeroID
        containerViewOutputPicture.heroID = HeroIDs.taskImageHeroID
    }
    
    @objc private func panAction(_ sender:UIPanGestureRecognizer) {
        let traslation = sender.translation(in: nil)
        let progress = traslation.y / 2 / view.bounds.height
        switch sender.state {
        case .began:
            hero_dismissViewController()
        case .changed:
            Hero.shared.update(progress)
            
            /// Position of all the elements to track their traslation during the pan gesture
            let currentViewPosition = CGPoint(x:  view.center.x, y: traslation.y + view.center.y)
            let currentImageBgPosition = CGPoint(x:  headerBackgroundView.center.x, y: traslation.y + headerBackgroundView.center.y)
            let currentContainerViewPicturePos = CGPoint(x:  containerViewOutputPicture.center.x, y: traslation.y + containerViewOutputPicture.center.y)

            /// Apply change of positions for each element
            Hero.shared.apply(modifiers: [.position(currentViewPosition)], to: view)
            Hero.shared.apply(modifiers: [.position(currentImageBgPosition)], to: headerBackgroundView)
            Hero.shared.apply(modifiers: [.position(currentContainerViewPicturePos)], to: containerViewOutputPicture)
        default:
            if progress + sender.velocity(in: nil).y / view.bounds.height > 0.2 {
                Hero.shared.finish()
            } else {
                Hero.shared.cancel()
            }
        }
    }
}
