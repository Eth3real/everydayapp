//
//  DrawingVC.swift
//  EverydayApp
//
//  Created by Simone Formisano on 22/05/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import UIKit

class DrawingVC: UIViewController {
    @IBOutlet weak var doneButton: CustomButton!
    @IBOutlet var drawingView: DrawingView!
    @IBOutlet weak var finalImage: UIImageView!
    
    var todaysChallenge: Entity!
    var comingFromHistory = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        doneButton.isEnabled = false
        doneButton.addTarget(self, action: #selector(donePressed), for: .touchUpInside)
        if !comingFromHistory {
            todaysChallenge = CoreDataController().getTodaysChallenge()
        }
        
        if todaysChallenge.done {
            drawingView.backgroundColor = .clear
            finalImage.alpha = 1.0
            doneButton.alpha = 0.0
            drawingView.isUserInteractionEnabled = false
            finalImage.image = UIImage(data: todaysChallenge.resultImage!)
             view.setNeedsDisplay()
        } else {
            finalImage.alpha = 0.0
            doneButton.alpha = 1.0
            drawingView.isUserInteractionEnabled = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !comingFromHistory {
            todaysChallenge = CoreDataController().getTodaysChallenge()
        }
        
        if todaysChallenge.done {
            drawingView.backgroundColor = .clear
            finalImage.alpha = 1.0
            doneButton.alpha = 0.0
            drawingView.isUserInteractionEnabled = false
            finalImage.image = UIImage(data: todaysChallenge.resultImage!)
            view.setNeedsDisplay()
        } else {
            finalImage.alpha = 0.0
            doneButton.alpha = 1.0
            drawingView.isUserInteractionEnabled = true
        }
    }
    
    @objc private func donePressed() {
        if drawingView.preRenderImage != nil {
        let parent = self.parent as? ExpandedTaskVC
        parent?.executeSegueToCongrats()
            todaysChallenge.setValue(true, forKey: "done")
        todaysChallenge.setValue(drawingView.preRenderImage.jpegData(compressionQuality: 0.8), forKey: "resultImage")
        todaysChallenge.setValue(Date(), forKey: "whenDone")
        try? todaysChallenge.managedObjectContext?.save()
        drawingView.isUserInteractionEnabled = false
        doneButton.alpha = 0.0
        parent?.challengeDescription.text = "Challenge done!"
    }
    }
}
