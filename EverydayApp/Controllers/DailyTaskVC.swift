//
//  ViewController.swift
//  HeroTransition_test-cardView
//
//  Created by Simone Formisano on 12/05/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import UIKit
import Hero
import MessageUI
import CoreData

class DailyTaskVC: UIViewController {
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var lblTextUs: UILabel!
    @IBOutlet weak var cardBackgroundView: UIView!
    @IBOutlet weak var taskTitle: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var helloUsername: UILabel!
    @IBOutlet weak var goTodayChallengeLabel: UILabel!
    @IBOutlet weak var challengesLeftLabel: UILabel!
    @IBOutlet weak var topCircleView: UIView!
    @IBOutlet weak var lineHiderView: UIView!
    @IBOutlet weak var circleImage: UIImageView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var spiralBtnImage: UIImageView!
    @IBOutlet weak var challengeBtnView: UIView!
    private var months = CoreDataController.shared.loadAllMonths()
    private var todaysChallenge: Entity!
    var passedUsernameString: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUsername()
        // adding months
        addMonths()

        // checks if there are months to add
        checkChallengesToAdd()
        
        correctUnsavedCompletedChallengeDates()
        months = CoreDataController.shared.loadAllMonths()
        todaysChallenge = CoreDataController().getTodaysChallenge()
        print("prima inizialize")
        inizialize()
        updateProgressBar()
        setupTransparentNavigationBar()
    }
    
    private func correctUnsavedCompletedChallengeDates() {
        let allChallenges = CoreDataController.shared.loadAllChallenges()
        let allChallengesDoneWhithoutASavedDate = allChallenges.filter{$0.done == true && $0.whenDone == nil}
        
        print(allChallengesDoneWhithoutASavedDate)

        for challenge in allChallengesDoneWhithoutASavedDate {
            print(challenge)
            challenge.setValue(Date(), forKey: "whenDone")
            try? challenge.managedObjectContext?.save()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if todaysChallenge.done {
            goTodayChallengeLabel.text = "Challenge completed"
            challengeBtnView.alpha = 0.65
        } else {
            goTodayChallengeLabel.text = "Go to today's challenge"
            challengeBtnView.alpha = 1.0
        }
        profileImage.image = CoreDataController.shared.takePhotoSaved()
        profileImage.layer.cornerRadius = profileImage.bounds.width / 2
        profileImage.clipsToBounds = true
        updateProgressBar()
        setUsername()
        
        if allChallengesCompleted() {
            if !popupShowed() {
                print(popupShowed())
                performSegue(withIdentifier: "goPopup", sender: self)
                UserDefaults.standard.set(true, forKey: "popupShowed")
            }
        } else {
            UserDefaults.standard.set(false, forKey: "popupShowed")
        }
    }
    
    private func popupShowed() -> Bool {
          return UserDefaults.standard.bool(forKey: "popupShowed")
    }
    
    // Returns true if all the challenges of the month have been completed
    private func allChallengesCompleted() -> Bool {
        let allChallenges = CoreDataController.shared.loadAllChallenges()
        let allChallengesOftheMonth = allChallenges.filter{Date.extractMonth(date: $0.day!) == Date.extractMonth(date: Date()) }
        let allChallengesDoneOfTheMonth = allChallengesOftheMonth.filter{$0.done == true}
        
        return allChallengesOftheMonth.count == allChallengesDoneOfTheMonth.count ? true : false
    }
    
    private func setupTransparentNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(color: .white), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage(color: .white)
        self.navigationController?.view.backgroundColor = .white
    }
    
    private func updateProgressBar() {
        let lastDayOfMonth = Date.getLastDayOfTheMonth()
        
        /// The number of challenge done in this month
        let numberOfChallengesDone = CoreDataController.shared.loadAllChallenges().filter({$0.done == true && Date.extractMonth(date: $0.day!) == Date.extractMonth(date: Date())}).count
        
        if numberOfChallengesDone != 0 {
            progressBar.progress = ((100 / Float(lastDayOfMonth)) * Float(numberOfChallengesDone)) / 100
        }
        
        let challengeLeft = lastDayOfMonth - numberOfChallengesDone
        
        if challengeLeft != 0 {
            let challengeWord = challengeLeft == 1 ? "challenge" : "challenges"
            challengesLeftLabel.text = "\(challengeLeft) \(challengeWord) left to reach your goal!"
        } else {
            challengesLeftLabel.text = "Congratulations, you achieved a whole month of challenges!"
        }
        
        if challengeLeft == 30 {
            progressBar.progress = 0.0
        }
    }
    
    private func setUsername() {
         let userDefault = UserDefaults.standard 
    
         if userDefault.string(forKey: "username") == nil {
             UserDefaults.standard.set(passedUsernameString, forKey: "username")
             helloUsername.text = "Hello there, \(passedUsernameString)!"
         } else {
             helloUsername.text = "Hello there, \(userDefault.string(forKey: "username")!)!"
         }
    }
    
    private func inizialize() {
        print("initializieieiieieiei")
        let tapGestureCard = UILongPressGestureRecognizer(target: self, action: #selector(tapAction))
        let tapGestureEmail = UITapGestureRecognizer(target: self, action: #selector(triggerSendEmail))
        
        profileImage.image = CoreDataController.shared.takePhotoSaved()
        profileImage.layer.cornerRadius = profileImage.bounds.width / 2
        profileImage.clipsToBounds = true
        
        tapGestureCard.minimumPressDuration = 0.0
        challengeBtnView.addGestureRecognizer(tapGestureCard)
        lblTextUs.isUserInteractionEnabled = true
        lblTextUs.addGestureRecognizer(tapGestureEmail)
    
        
        designProgressBar()
        setCardHeroIDs()
        
        let monthChallenge = months.filter({Date.extractMonth(date: $0.month!) == Date.extractMonth(date: Date())}).first
        print("month challenge is \(monthChallenge)")
        taskTitle.text = monthChallenge?.monthTitle
        print(taskTitle)
        print(monthChallenge?.monthImage)
        circleImage.image = UIImage(data: (monthChallenge?.monthImage)!)
        
        topCircleView.layer.cornerRadius = topCircleView.bounds.width / 2
        //        circleImage.layer.cornerRadius = circleImage.bounds.width / 2
        circleImage.backgroundColor = .clear
        topCircleView.layer.borderWidth = 1.5
        topCircleView.layer.borderColor = .init(srgbRed: 0, green: 0, blue: 0, alpha: 1)
        
        cardView.layer.borderColor = .init(srgbRed: 0, green: 0, blue: 0, alpha: 1.0)
        cardView.layer.borderWidth = 1.5
        cardView.layer.cornerRadius = 15
        lineHiderView.layer.cornerRadius = lineHiderView.bounds.width / 2
        circleImage.layer.cornerRadius = circleImage.bounds.width / 2
        circleImage.clipsToBounds = true
        circleImage.sizeToFit()

    }

    /// Passing the daily challenge
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToExpandedTaskSegue" {
            let destinationVC = segue.destination as! ExpandedTaskVC
            destinationVC.todaysChallenge = todaysChallenge
        }
    }
    
    private func designProgressBar() {
        progressBar.layer.cornerRadius = 7
        progressBar.clipsToBounds = true
        progressBar.layer.borderWidth = 1;
        progressBar.layer.borderColor = .init(srgbRed: 122/255, green: 119/255, blue: 246/255, alpha: 1.0)
    }
    
    private func setCardHeroIDs() {
        /// Setting Hero framework IDs
        //cardView.heroID = HeroIDs.taskCardViewHeroID
        challengeBtnView.heroID = HeroIDs.taskBackgroundImageID
        goTodayChallengeLabel.heroID = HeroIDs.taskTitleHeroID
        spiralBtnImage.heroID = HeroIDs.taskImageHeroID
    }
    
    @objc private func triggerSendEmail() {
        sendEmail()
    }
    
    @objc private func tapAction(_ sender: UILongPressGestureRecognizer) {
        if sender.state == .began {
            UIView.animate(withDuration: 0.2, animations: {
                self.challengeBtnView.transform = CGAffineTransform(scaleX: 0.90, y: 0.90)
            })
        } else if sender.state == .cancelled || sender.state == .ended {
            UIView.animate(withDuration: 0.2, animations: {
                self.challengeBtnView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
            performSegue(withIdentifier: "goToExpandedTaskSegue", sender: self)
        }
    }
}

extension DailyTaskVC: MFMailComposeViewControllerDelegate {
    private func sendEmail() {
        print(MFMailComposeViewController.canSendMail())
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["appsternachallenges@gmail.com"])
            mail.setMessageBody("<p>Hi there,<br> I'd like to suggest you these new challeneges:</p>", isHTML: true)
            present(mail, animated: true)
            mail.view.tintColor = UIColor(red: 122/255, green: 119/255, blue: 246/255, alpha: 1)
            
        } else {
            let alert = UIAlertController(title: "Missing e-mail account", message: "In order to suggest us new ideas you have to configure at least one e-mail account in you iPhone settings.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Got it", style: .default, handler: nil))
            alert.view.tintColor = UIColor(red: 122/255, green: 119/255, blue: 246/255, alpha: 1)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    internal func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    @IBAction func unwindToMain(_ segue: UIStoryboardSegue) {
    }
    
    @IBAction func unwindToMainFromHistory(_ segue: UIStoryboardSegue) {
        
    }
}

