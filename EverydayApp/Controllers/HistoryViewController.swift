//
//  HistoryViewController.swift
//  EverydayApp
//
//  Created by FrancescaCiancio on 12/05/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import UIKit
var counter = 0
class HistoryViewController: UIViewController {
    var challenges = CoreDataController.shared.loadAllChallenges()
    @IBOutlet var scrollView: UIScrollView!
    var daysToShow : Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        Date().missingDays()
        print(counter)
        daysToShow = challenges.count + counter
        scrollView.contentSize.height = CGFloat(270*(2 + (daysToShow/7)))//need to be fixed a bit
        createGroup()
    }
    //    this function create the structure of the 7 (or less) circles
    func createGroup() {
        var x : Int!
        var y: Int!
        for indice in stride(from: 0, to: daysToShow, by: +7) {
            let numberOfTimes = indice/7
            for i in 0...6 {
                if (i+indice) == daysToShow {
                    break
                }
                let myView = UIView()
                if (i+indice) < challenges.count{
                    setUIView(myView: myView, isDone: challenges[i + indice].done)
                } else{
                    setUIView(myView: myView, isDone: false)
                }
                //                to give the right position for every circle
                switch i {
                case 0: x = -75; y = 0
                case 1: x = 15; y = 0
                case 2: x = -120; y = 90
                case 3: x = -30; y = 90
                case 4: x = 60; y = 90
                case 5: x = -75; y = 180
                case 6: x = 15; y = 180
                default:
                    print("i is \(i)")
                }
                myView.frame = CGRect(x: Int(scrollView.frame.size.width)/2 + x, y: 100 + y + numberOfTimes*( 300 ), width: 60, height: 60)
                scrollView.addSubview(myView)
                let label = UILabel()
                addlabel(j: (indice + i ), label: label)
                label.frame = myView.frame
                scrollView.addSubview(label)
            }
        }
    }
    //    this function set and add the label on the circle
    func addlabel(j: Int, label: UILabel ){
        label.textAlignment = .center
        label.textColor = .white
        label.font = .boldSystemFont(ofSize: 25)
        DateFormatter().dateFormat = "dd"
        var text = ""
        if j >= challenges.count {
            text = "🔒"
        } else{
            text = DateFormatter().string(from: challenges[j].day!)
        }
        label.text = text
        scrollView.addSubview(label)
    }
    //    this function set the circle and its color
    func setUIView(myView: UIView, isDone: Bool){
        myView.layer.borderWidth = 3
        myView.layer.cornerRadius = 30
        if isDone {
            myView.backgroundColor = .systemYellow
            myView.layer.borderColor = #colorLiteral(red: 0.7254902124, green: 0.4784313738, blue: 0.09803921729, alpha: 1)
        } else {
            myView.backgroundColor = .gray
            myView.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        }
    }
    
}
extension Date {
    //    this function return the last day of the current month
    func lastDayOfTheMonth() -> DateComponents {
        let components:NSDateComponents = Calendar.current.dateComponents([.year, .month], from: self) as NSDateComponents
        components.month += 1
        components.day = 1
        components.day -= 1
        let last = Calendar.current.date(from: components as DateComponents)!
        return Calendar.current.dateComponents(in: .current, from: last)
    }
    //    this function calculate how many days do we miss until the end of the month
    func missingDays()  {
        let components:NSDateComponents = Calendar.current.dateComponents([.day], from: self) as NSDateComponents
        let endDay = lastDayOfTheMonth().day
        while (components.day < endDay!){
            components.day += 1
            counter += 1
        }
    }
    
}
