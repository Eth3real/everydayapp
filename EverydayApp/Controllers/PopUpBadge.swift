//
//  PopUpViewController.swift
//  EverydayApp
//
//  Created by FrancescaCiancio on 15/06/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import UIKit

class PopUpBadge: UIViewController {
var i = Int()
    var image = UIImage()
    var label = UILabel()
    var popUp = UIView()
    var blurView = UIVisualEffectView()
    var images = [UIImage(named: "trick-Complete"),UIImage(named: "cook-complete"),UIImage(named:"user-comple"),UIImage(named: "minimalism-complete"),UIImage(named: "nonDomain-Complete"),UIImage(named: "oneThing-complete"), UIImage(named: "trick-Complete"),UIImage(named: "cook-complete"),UIImage(named:"user-comple"),UIImage(named: "minimalism-complete"),UIImage(named: "nonDomain-Complete"),UIImage(named: "oneThing-complete")]
    override func viewDidLoad() {
        super.viewDidLoad()
        print("in popUpBadge")
        view.backgroundColor = UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 0.5)
        showPopUp()
print("lalalallalalalalallallalallalalal")
        // Do any additional setup after loading the view.
    }
    func showPopUp() {
        setPopUp(popUp: popUp)
        addCloseButton()
        let monthTitle = MyLabel(labelTitle: label.text! ,lines: 2, labelFont: .custom, color: .black, allignment: .center)
              monthTitle.frame = CGRect(x: 10, y: 40, width: popUp.frame.width-20, height: 100)
        monthTitle.font = UIFont(name: "SportingGrotesque-Regular", size: 30)
        popUp.addSubview(monthTitle)
        let allMonths = CoreDataController.shared.loadAllMonths()
        let month = (Calendar.current.dateComponents(in: .current, from: allMonths[i].month!) as NSDateComponents).month
        print("ciao popUp")
        print(month)
        showImage(i: month)
    }
        func addCloseButton() {
            let close = UIButton()
            close.setTitle("X", for: .normal)
            close.setTitleColor(.systemGray, for: .normal)
            popUp.addSubview(close)
            close.frame = CGRect(x:  popUp.frame.width - 60, y: 10, width: 50, height: 50)
            close.addTarget(self, action: #selector(closePopUP), for: .touchUpInside)
        }
        @objc func closePopUP() {
            dismiss(animated: true, completion: nil)
            view.willRemoveSubview(blurView)
        }
       
        func showImage(i: Int) {
            let imageView = UIImageView()
            imageView.frame = CGRect(x: 20, y: 150 , width: popUp.bounds.width - 60 , height:  popUp.frame.width - 60)
//            let image = images[i]
           
            imageView.image = image
            imageView.contentMode = .scaleAspectFit
            popUp.addSubview(imageView)
        }
        
        func setPopUp(popUp: UIView) {
            view.addSubview(popUp)
            popUp.backgroundColor = .white
            popUp.layer.cornerRadius = 20
            popUp.frame = CGRect(x: 20, y: 0, width: view.frame.width - 80, height: view.frame.height*3/5)
            popUp.center = view.center
        }
       
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
