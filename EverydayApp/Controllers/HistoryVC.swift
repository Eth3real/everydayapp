//
//  StoricoViewController.swift
//  EverydayApp
//
//  Created by FrancescaCiancio on 11/05/2020.
//  Copyright © 2020 Francesca Ciancio. All rights reserved.
//

import UIKit

class HistoryVC: UIViewController {
    
    var challenges = CoreDataController.shared.loadAllChallenges()
    var currentSelectedChallenge: Entity!
    var item = 0
    @IBOutlet var scrollView: UIScrollView!
    var daysToShow : Int!
    var rectangle: UIView!
    var buttomLabel: UILabel!
    var height: CGFloat = 0
    let formatter = DateFormatter()
    var bottomOffset: CGPoint!
    var topLabel: UILabel!
    var itemToGo = 0

    @IBOutlet weak var gotcha: UIButton!
    @IBOutlet var popUpView: UIView!
//    @IBOutlet weak var popUp: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        addPopUpView()
        
        print(challenges.count)
        inizialize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // It's necessary to inizialize even in this lyfe cycle state to avoid the view not updating when loadet yet
        //        inizialize()
        addCircle()
        
        if UserDefaults.standard.bool(forKey: "Pop Up History Appeared") {

            popUpView.removeFromSuperview()

        }

        if UserDefaults.standard.bool(forKey: "Pop Up History Appeared") {

            print("Aleardy Appeared")

        } else {

            addPopUpView()
            UserDefaults.standard.set(true, forKey: "Pop Up History Appeared")

        }
        
    }
    
    func addPopUpView() {
  
       
                   
//        popUpView.frame = CGRect(x: 0, y: -88, width: 414, height: 1200)
//
//        popUp.frame = CGRect(x: 20, y: 240, width: 374, height: 426)
        popUpView.frame = CGRect(x: 0, y: 0, width: view.frame.width*6/7, height: view.frame.height*3/7)
        popUpView.layer.cornerRadius = 15
               popUpView.center = view.center
//        popUp.frame = popUpView.frame
        view.addSubview(popUpView)
//               view.addSubview(popUp)
        gotcha.layer.cornerRadius = 20
        

    }

    
    @IBAction func gotchaBtn(_ sender: UIButton) {

        popUpView.removeFromSuperview()
//        popUp.removeFromSuperview()

    }
    
    func inizialize() {
        scrollView.delegate = self
        //        print("number of challenges")
        //        print(challenges.count)
        daysToShow = challenges.count - Date().missingDays()
        print("days to show")
        print(daysToShow!)
        formatter.dateFormat = "MMMM"
        
        let size = (view.frame.height)*2/7
        height = UIApplication.shared.statusBarFrame.height +
            self.navigationController!.navigationBar.frame.height
        print(height)
        scrollView.frame = CGRect(x: 0, y: height + 30 , width: view.frame.width, height: view.frame.height - size + 4)
        scrollView.contentMode = UIView.ContentMode.redraw
        scrollView.contentSize.height =  CGFloat( 120*(daysToShow))
        bottomOffset = CGPoint(x: 0, y: scrollView.contentSize.height - scrollView.frame.height )
        if item != 0 {
            bottomOffset = CGPoint(x: 0, y: CGFloat( 120*(item)) - scrollView.frame.height )
        }
        print(item)
        scrollView.setContentOffset(bottomOffset, animated: true)
        //        scrollView.backgroundColor = .blue
        
        addLine()
        addCircle()
        addTopLabel()
    }
    func addTopLabel() {
        var month = challenges[daysToShow-1].day!
        itemToGo = challenges.count - 1
        if item != 0 {
            month = challenges[item - 1].day!
            itemToGo = item - 1
        }
        topLabel = MyLabel(labelTitle: formatter.string(from: month), labelFont: .bold, allignment: .center)
        topLabel.frame = CGRect(x: view.frame.width/2 - 50, y: height + 10, width: 100, height: 20)
        view.addSubview(topLabel)
        addButtonArrows()
    }
    func addButtonArrows() {
        let left = UIButton()
        left.frame = CGRect(x: view.frame.width/2 - 70, y: height + 10, width: 20, height: 20)
        left.setImage(#imageLiteral(resourceName: "left"), for: .normal)
        left.setTitleColor(.black, for: .normal)
        view.addSubview(left)
        left.addTarget(self, action: #selector(previousMonth), for: .touchUpInside)
        let right = UIButton()
        right.frame = CGRect(x: view.frame.width/2 + 50, y: height + 10, width: 20, height: 20)
        right.setImage(#imageLiteral(resourceName: "right"), for: .normal)
        right.setTitleColor(.black, for: .normal)
        view.addSubview(right)
        right.addTarget(self, action: #selector(nextMonth), for: .touchUpInside)
    }
    @objc func nextMonth() {
        print("in next month")
        print(itemToGo)
        if itemToGo != challenges.count - 1 {
            let plus = Date().MonthComponents(date:challenges[itemToGo + 1].day!).day!
            print("plus is \(plus)")
            itemToGo += plus
        }
        if itemToGo < daysToShow {
            bottomOffset = CGPoint(x: 0, y: CGFloat( 120*(itemToGo + 1)) - scrollView.frame.height )
            topLabel.text = formatter.string(from: challenges[itemToGo].day!)
            item = -1
        }
        else {
            bottomOffset = CGPoint(x: 0, y: scrollView.contentSize.height - scrollView.frame.height )
            topLabel.text = formatter.string(from: challenges[daysToShow].day!)
            item = 0
            itemToGo = challenges.count - 1
        }
        scrollView.setContentOffset(bottomOffset, animated: true)
    }
    @objc func previousMonth() {
        print(itemToGo)
        print("in previous month")
        let firstMonth = Date().MonthComponents(date: challenges[0].day!).day!
        let mainless = Date().MonthComponents(date:challenges[itemToGo].day!).day!
        if challenges.count - 1 != firstMonth - 1 {
            print(" mainless is \(mainless)")
            itemToGo -= mainless
            if itemToGo > firstMonth {
                bottomOffset = CGPoint(x: 0, y: CGFloat( 120*(itemToGo + 1)) - scrollView.frame.height )
                topLabel.text = formatter.string(from: challenges[itemToGo].day!)
                item = -1
            } else {
                bottomOffset = CGPoint(x: 0, y: CGFloat(120*(firstMonth)) - scrollView.frame.height )
                topLabel.text = formatter.string(from: challenges[firstMonth - 1].day!)
                item = -1
                itemToGo = firstMonth - 1
            }
        }else {
            bottomOffset = CGPoint(x: 0, y: scrollView.contentSize.height - scrollView.frame.height )
            topLabel.text = formatter.string(from: challenges[daysToShow].day!)
            item = 0
            itemToGo = challenges.count - 1
        }
        
        scrollView.setContentOffset(bottomOffset, animated: true)
        print(itemToGo)
        
    }
    
    func addLine(){
        
        let line = createLine(color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        line.frame = CGRect(x: Int(view.frame.size.width)/2 - 2, y: Int(height + 30), width: 4, height:  Int(view.frame.height)  - Int(view.frame.height)*2/7)
        let rightArrow = createLine(color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        
        let xArrowR: CGFloat = view.frame.size.width/2 - 4
        let yArrowR: CGFloat = height + 30 + CGFloat((view.frame.height) - (view.frame.height)*2/7 - 5)
        
        rightArrow.frame = CGRect(x: xArrowR, y: yArrowR, width: 20, height: 4)
//        rightArrow.frame = CGRect(x: view.frame.size.width/2 - 4, y: height + 30 + CGFloat(view.frame.height) - (view.frame.height)*2/7 - 5, width: 20, height: 4)
        rotateline(line: rightArrow, angle: .pi/(-4))
        let leftArrow = createLine(color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        
        let xArrowL: CGFloat = view.frame.size.width/2 - 16
        let yArrowL: CGFloat = height + 30 + CGFloat((view.frame.height) - (view.frame.height)*2/7 - 5)
        
        leftArrow.frame = CGRect(x: xArrowL, y: yArrowL, width: 20, height: 4)
//        leftArrow.frame = CGRect(x: view.frame.size.width/2 - 16, y: height + 30 + CGFloat(view.frame.height) - (view.frame.height)*2/7 - 5, width: 20, height: 4)
        rotateline(line: leftArrow, angle: .pi/4)
        rectangle = createLine(color: #colorLiteral(red: 0.4777091742, green: 0.4671983719, blue: 0.9636408687, alpha: 1))
        rectangle.frame = CGRect(x: 0, y: Int(height + 32) + Int(view.frame.height) + 50, width: Int(view.frame.width), height: Int(view.frame.height)*2/7)
        buttomLabel = MyLabel(labelTitle: "You're done for the day! \n Wait until tomorrow for the next challenge.",lines: 3, labelFont: .regular, color: .white, allignment: .center)
        buttomLabel.font = UIFont(name: "SF Compat Display"  , size: 17)
        buttomLabel.frame = CGRect(x: 0, y: height + 30 +  view.frame.height + 50 , width: view.frame.width, height: 80)
        
        /// Gradient to make the bottom rettangle fade away when the user scrolls
        let gradientMaskLayer = CAGradientLayer()
        gradientMaskLayer.frame = rectangle.bounds
        gradientMaskLayer.colors = [UIColor.white.cgColor, UIColor.clear.cgColor]
        gradientMaskLayer.locations = [0.0, 1.0]
        rectangle.layer.mask = gradientMaskLayer
        /// End gradinet masking
        
        view.addSubview(line)
        view.addSubview(rightArrow)
        view.addSubview(leftArrow)
        view.addSubview(scrollView)
        for i in 0...4 {
            let tratteggio = createLine(color: #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1))
            tratteggio.frame  = CGRect(x: Int(view.frame.size.width)/2 - 1, y: Int(view.frame.height) - Int(view.frame.height)*2/7 + Int(height + 30) + 4 + i*30, width: 2, height:  15)
            view.addSubview(tratteggio)
        }
        
        view.addSubview(rectangle)
        view.addSubview(buttomLabel)
        
    }
    func createLine(color: UIColor) -> UIView {
        let line = UIView()
        line.backgroundColor = color
        return line
    }
    func rotateline(line: UIView, angle: CGFloat) {
        UIView.animate(withDuration: 0.0) {
            let rotatefotoTransform = CGAffineTransform(rotationAngle: angle)
            line.transform = rotatefotoTransform
        }
    }
    
    func addCircle() {
        for i in 0 ..< daysToShow {
            setButton(i: i )
            addlabels(i: i )
        }
    }
    
    func addlabels(i: Int){
        let formatter = DateFormatter()
        formatter.dateStyle = DateFormatter.Style.medium
        let date = formatter.string(from: challenges[i].day!)
        let text  = challenges[i].challenge!
        let dateLabel = MyLabel( labelTitle: "\(date)", labelFont: .regular)
        let challengeLabel = MyLabel( labelTitle: "\(text)", lines: 3, labelFont: .bold)
        
        challengeLabel.tag = i
        if i%2 == 0 {
            dateLabel.frame = CGRect(x: Int(view.frame.size.width)/2 + 40, y: 10 + i*( 120 ) - 10 , width: Int(view.frame.size.width)/2 - 55, height: 30)
            challengeLabel.frame = CGRect(x: Int(view.frame.size.width)/2 + 40, y: 20 + i*( 120 )  , width: Int(view.frame.size.width)/2 - 55, height: 40 + 20)
        } else {
            dateLabel.frame = CGRect(x: 15 , y: 10 + i*( 120 ) - 10 , width:Int(view.frame.size.width)/2 - 55, height: 30)
            challengeLabel.frame = CGRect(x: 15, y: 20 + i*( 120 ) , width: Int(view.frame.size.width)/2 - 55, height: 40 + 20)
            challengeLabel.textAlignment = .right
            dateLabel.textAlignment = .right
        }
        scrollView.addSubview(dateLabel)
        scrollView.addSubview(challengeLabel)
    }
    
    func setButton(i: Int){
        let isDone = challenges[i].done
        let myButton = UIButton()
        myButton.tag = i
        myButton.layer.borderWidth = 3
        myButton.layer.cornerRadius = 30
        myButton.layer.borderColor = #colorLiteral(red: 0.4777091742, green: 0.4671983719, blue: 0.9636408687, alpha: 1)
        if isDone {
            myButton.layer.backgroundColor = myButton.layer.borderColor
        } else {
            myButton.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            if formatter.string(from: challenges[i].day!) == formatter.string(from: Date()){
                
                addImage(button: myButton)
            }
        }
        myButton.frame = CGRect(x: Int(view.frame.size.width)/2 - 30, y: 10 + i*( 120 ), width: 60, height: 60)
        scrollView.addSubview(myButton)
        myButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        //        let tapGestureButton = UILongPressGestureRecognizer(target: self, action: #selector(buttonAction))
        //        tapGestureButton.minimumPressDuration = 0.0
        //        myButton.addGestureRecognizer(tapGestureButton)
    }
    func addImage(button: UIButton) {
        let image = UIImage(named: "forHistory")
        button.setImage(image, for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12)
    }
    
    @objc func buttonAction(button: UIButton, _ sender: UILongPressGestureRecognizer) {
        button.heroID = HeroIDs.taskBackgroundImageID
        // extract the challenege title for the selected button
        let subviews = button.superview?.subviews
        var rightLabel: UILabel?
        
        for subview in subviews! {
            if subview is UILabel {
                if subview.tag == button.tag {
                    rightLabel = (subview as! UILabel)
                    break
                }
            }
        }
        
        let selectedChallengeTitle = rightLabel!.text
        let challenges = CoreDataController.shared.loadAllChallenges()
        
        
        // Getting from coredata the exact challenge with the same title
        currentSelectedChallenge = challenges.filter {$0.challenge == selectedChallengeTitle}.first
        
        if currentSelectedChallenge == nil {
            currentSelectedChallenge = challenges[0]
        }
        UIView.animate(withDuration: 0.05, animations: {
            button.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
        self.performSegue(withIdentifier: "goToOutput", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier  == "goToOutput") {
            let destinationVC = segue.destination as! ExpandedTaskVC
            print("going to \(destinationVC)")
            destinationVC.todaysChallenge = currentSelectedChallenge
            destinationVC.comingFromHistory = true
        }
    }
}
// MARK: - Extension ScrollViewDelegate

extension HistoryVC: UIScrollViewDelegate {
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        if item == 0 || item == daysToShow {
            if challenges[daysToShow - 1].done == true {
            Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(translate), userInfo: nil, repeats: false)
            
            Timer.scheduledTimer(timeInterval: 4.5, target: self, selector: #selector(translateBack), userInfo: nil, repeats: false)
        }
        }
    }
    @objc func translate() {
        UIView.animate(withDuration: 1.5){
            self.rectangle.transform = CGAffineTransform(translationX: 0, y: -(self.view.frame.height)*2/7 - 48)
            self.buttomLabel.transform = CGAffineTransform(translationX: 0, y: -(self.view.frame.height)*2/7 - 48)
        }
    }
    @objc func translateBack() {
        UIView.animate(withDuration: 1.5){
            self.rectangle.transform = CGAffineTransform(translationX: 0, y: (self.view.frame.height)*2/7 + 48)
            self.buttomLabel.transform = CGAffineTransform(translationX: 0, y: (self.view.frame.height)*2/7 + 48)
        }
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            self.stoppedScrolling()
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.stoppedScrolling()
    }
    
    func stoppedScrolling() {
        print("Scroll finished")
        print(Int(scrollView.contentOffset.y + scrollView.frame.height )/120)
        let k = Int(scrollView.contentOffset.y + scrollView.frame.height )/120
        topLabel.text = formatter.string(from: challenges[k].day!)
        let endMonth = Date().MonthComponents(date: challenges[k].day!).day!
        let dayScroll = (Calendar.current.dateComponents(in: .current, from: challenges[k].day!) as NSDateComponents).day
        itemToGo = k + endMonth - dayScroll
        print(itemToGo)
    }
//    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//        topLabel.text = ""
//    }
}



