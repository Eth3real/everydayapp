//
//  StatsVC.swift
//  EverydayApp
//
//  Created by FrancescaCiancio on 27/05/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import UIKit
import Charts
class StatsVC: UIViewController, ChartViewDelegate {
    
    @IBOutlet var myLabel: UILabel!
    let pieChart = PieChartView()
    let barChart = BarChartView()
    let challenges = CoreDataController.shared.loadAllChallenges()
    let formatter = DateFormatter()
    override func viewDidLoad() {
        super.viewDidLoad()
        pieChart.delegate = self
        barChart.delegate = self
        formatter.dateStyle = DateFormatter.Style.medium
        BarChart()
        //        addPieChart()
        myLabel.font = UIFont(name: "SportingGrotesque-Regular", size: 30)
        
    }
    
    func addPieChart(){
        pieChart.frame =  CGRect(x: 0, y: 0 , width: self.view.frame.width, height: self.view.frame.width)
        pieChart.center = view.center
        let dataEntries = [
            PieChartDataEntry(value: Double(15), label: ""),
            PieChartDataEntry(value: Double(40), label: ""),
            PieChartDataEntry(value: Double(70), label: "")
        ]
        
        let dataSet = PieChartDataSet(entries: dataEntries)
        dataSet.setColors([NSUIColor(#colorLiteral(red: 0.4777091742, green: 0.4671983719, blue: 0.9636408687, alpha: 1)),NSUIColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)),NSUIColor(#colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1))], alpha: 0.3)
        let data = PieChartData(dataSet: dataSet)
        pieChart.data = data
        pieChart.legend.enabled = false
        pieChart.rotationEnabled = false
        pieChart.highlightPerTapEnabled = false
        pieChart.usePercentValuesEnabled = true
        view.addSubview(pieChart)
    }
    func BarChart() {
        barChart.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height/2)
        barChart.center = view.center
        var dataEntries : [BarChartDataEntry] = []// i could add string
        var counterX: Double = 0
        for i in 0 ..< getItemOfToday() {
            let counterY =  getChallengesDone(date: challenges[i].day!)
            counterX += 1
            dataEntries.append(BarChartDataEntry(x: counterX, y: counterY))
            print(counterY)
        }
        print(dataEntries.count)
        let dataSet = BarChartDataSet(entries: dataEntries)
        dataSet.valueFormatter = DataSetValueFormatter()
        dataSet.setColor(NSUIColor(#colorLiteral(red: 0.4777091742, green: 0.4671983719, blue: 0.9636408687, alpha: 1)))
        let data = BarChartData(dataSet: dataSet)
        barChart.data = data
        view.addSubview(barChart)
        
        barChart.legend.enabled = false
    }
    func getItemOfToday() -> Int {
        var j = -1
        let today = formatter.string(from: Date())
        for i in 0 ..< challenges.count {
            if formatter.string(from: challenges[i].day!)  == today {
                j = i
            }
        }
        return j + 1
    }
    
    func getChallengesDone(date: Date) -> Double {
        var doneCounter : Double = 0
        let day = formatter.string(from: date)
        for challenge in challenges {
            if challenge.whenDone != nil {
                if day == formatter.string(from: challenge.whenDone!) {
                    doneCounter += 1
                    print(challenge.whenDone!)
                }
            }
        }
        return doneCounter
    }
}
// MARK: - Formatter
class DataSetValueFormatter: IValueFormatter {
    func stringForValue(_ value: Double,
                        entry: ChartDataEntry,
                        dataSetIndex: Int,
                        viewPortHandler: ViewPortHandler?) -> String {
        ""
    }
}


