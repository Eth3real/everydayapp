//
//  SettingsVC.swift
//  EverydayApp
//
//  Created by Giada Ciotola on 25/05/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import UIKit
import UserNotifications
import CoreData

class SettingsVC: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate {
    
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var helloLabel: UILabel!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var toggleSwitch: UISwitch!
    var passedUsernameString: String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
                print("in settings")
        self.navigationController?.delegate = self
        usernameTextField.delegate = self
       
        profileImage.image = CoreDataController.shared.takePhotoSaved()
                        
        usernamePlaceholder()
        circleImageStyle()
                
        setupKeyboard()
        dismissKeyboard()
                
        let defaults = UserDefaults.standard
            
        if (defaults.object(forKey: "SwitchState") != nil) {
            toggleSwitch.isOn = defaults.bool(forKey: "SwitchState")
            if toggleSwitch.isOn == true {
                createNotifications()
                defaults.set(true, forKey: "SwitchState")
            } else {
                UNUserNotificationCenter.current().removeAllDeliveredNotifications()
                UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: ["uuidString"])
                UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["uuidString"])
                defaults.set(false, forKey: "SwitchState")
            }
        }

    }

    
    @IBAction func changePhotoButton(_ sender: Any) {
        
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.allowsEditing = true
        
        let alertController = UIAlertController(title: "Add an Image", message: "Choose From", preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (action) in
            pickerController.sourceType = .camera
            self.present(pickerController, animated: true, completion: nil)
            
        }
        
        let photosLibraryAction = UIAlertAction(title: "Photos Library", style: .default) { (action) in
            pickerController.sourceType = .photoLibrary
            self.present(pickerController, animated: true, completion: nil)
            
        }
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        alertController.addAction(cameraAction)
        alertController.addAction(photosLibraryAction)
        alertController.addAction(cancelAction)
        alertController.view.tintColor = UIColor(red: 122/255, green: 119/255, blue: 246/255, alpha: 1)
        
        present(alertController, animated: true, completion: nil)
        
        
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            if let image = info[.editedImage] as? UIImage {
              profileImage.image = image
               print("change image")
               CoreDataController.shared.changePhoto(image: profileImage.image!)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func switchToggle(_ sender: UISwitch) {

        let defaults = UserDefaults.standard

        if toggleSwitch.isOn {
            createNotifications()
            defaults.set(true, forKey: "SwitchState")

        } else {
            UNUserNotificationCenter.current().removeAllDeliveredNotifications()
            UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: ["uuidString"])
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["uuidString"])
            defaults.set(false, forKey: "SwitchState")

        }

    }
    

    func createNotifications() {

//        STEP 1: Ask for permission
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in

            }
//        STEP 2: Create the notification content
        let content = UNMutableNotificationContent()
        content.title = "We miss you!"
        content.body = "You haven't been very active this week. Come back for new challenges and make up for the time lost!"
        content.sound = UNNotificationSound.default
        content.categoryIdentifier = "7 days away"


//        STEP 3: Create the trigger
        
        let date = Date().addingTimeInterval(604800)

        let dateComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date)
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)

//        STEP 4: Create the request
        let uuidString = UUID().uuidString
        
        let request = UNNotificationRequest(identifier: uuidString, content: content, trigger: trigger)
        
//        STEP 5: Register the request
        center.add(request) { (error) in
            
        }

    }
    
    
    func usernamePlaceholder() {
         let userDefault = UserDefaults.standard
    
         if userDefault.string(forKey: "username") == nil {
             UserDefaults.standard.set(passedUsernameString, forKey: "username")
             usernameTextField.placeholder = "\(passedUsernameString)"
         } else {
             usernameTextField.placeholder = "\(userDefault.string(forKey: "username")!)"
         }
    }
    
    func circleImageStyle() {
        profileImage.layer.masksToBounds = true
        profileImage.layer.cornerRadius = profileImage.frame.height/2
        profileImage.clipsToBounds = true
    }
    
    
    private func setupKeyboard() {
         let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
         view.addGestureRecognizer(tap)
     }
       //    this function dismiss the keyboard by clicking the send button on it
       func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {
           self.view.endEditing(true)
           return true
       }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func clearChallenges(_ sender: Any) {
        let alertController = UIAlertController(title: "Do you want to delete all your challenges?", message: "By pressing 'Accept', you confirm you want to delete all the challenges you did.", preferredStyle: .alert)
        alertController.view.tintColor = UIColor(red: 122/255, green: 119/255, blue: 246/255, alpha: 1)


        let declineAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let acceptAction = UIAlertAction(title: "Accept", style: .default) { (_) -> Void in
            CoreDataController.shared.clearChallenges()
        }

        alertController.addAction(declineAction)
        alertController.addAction(acceptAction)

        present(alertController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "fromSettingToMain" {
            if !usernameTextField.text!.isEmpty {
                let userDefaultStore = UserDefaults.standard
                userDefaultStore.set(usernameTextField.text, forKey: "username")
            }
        }
    }
}

    



