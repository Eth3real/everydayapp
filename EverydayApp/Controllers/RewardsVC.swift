//
//  RewardsViewController.swift
//  EverydayApp
//
//  Created by FrancescaCiancio on 15/05/2020.
//  Copyright © 2020 Francesca Ciancio. All rights reserved.
//

import UIKit

var counter = 0

class RewardsVC: UIViewController {
    var height: CGFloat = 0
    var item: Int!
    var toDo : [Int] = []
    var done: Int!
    let date = Date()
    var today: NSDateComponents!
    var month: [NSDateComponents] = []
    var images = [UIImage(named: "trick-Complete"),UIImage(named: "cook-complete"),UIImage(named:"user-comple"),UIImage(named: "minimalism-complete"),UIImage(named: "nonDomain-Complete"),UIImage(named: "oneThing-complete"), UIImage(named: "trick-Complete"),UIImage(named: "cook-complete"),UIImage(named:"user-comple"),UIImage(named: "minimalism-complete"),UIImage(named: "nonDomain-Complete"),UIImage(named: "oneThing-complete")]
    @IBOutlet var scrollView: UIScrollView!
    
    let allMonths = CoreDataController.shared.loadAllMonths()
    let allChallenges =  CoreDataController.shared.loadAllChallenges()
    
    let formatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("che anno")
        print(allChallenges.last!.day)
        formatter.dateFormat = "MMMM"
        print(CoreDataController.shared.loadAllMonths())
        let todayComponent: NSDateComponents = Calendar.current.dateComponents(in: .current, from: Date()) as NSDateComponents
        
        let lastMonthComponent: NSDateComponents = Calendar.current.dateComponents(in: .current, from: allMonths.last!.month!) as NSDateComponents
        var difference: Int
        print(todayComponent.month)
        print(lastMonthComponent.month)
        if todayComponent.month > lastMonthComponent.month {
             difference = 12 - todayComponent.month + lastMonthComponent.month
            print("DIFFERENCE IS \(difference)")
        } else {
         difference = lastMonthComponent.month - todayComponent.month
            print("DIFFERENCE IS last - today \(difference)")
        }
       
//        calculate how many months i miss
        let monthsToShow = allMonths.count - difference
        scrollView.contentSize.height = CGFloat(130*( monthsToShow ) + 50)
        let navcontroller = self.navigationController ?? UINavigationController()
        height = UIApplication.shared.statusBarFrame.height + navcontroller.navigationBar.frame.height
        print("height is \(height)")
        scrollView.frame = CGRect(x: 0, y: height , width: view.frame.width, height: view.frame.height - height)
        scrollView.contentMode = UIView.ContentMode.redraw
        scrollView.contentSize.height =  CGFloat(130*( monthsToShow ))
        let bottomOffset = CGPoint(x: 0, y: scrollView.contentSize.height - scrollView.frame.height )
//        scrolla al mese di oggi
//        scrollView.setContentOffset(bottomOffset, animated: true)
        addElement()
    }
    
    func addElement() {
        let todayComponent: NSDateComponents = Calendar.current.dateComponents(in: .current, from: Date()) as NSDateComponents
        
        let lastMonthComponent: NSDateComponents = Calendar.current.dateComponents(in: .current, from: allMonths.last!.month!) as NSDateComponents
        var difference: Int
        print(todayComponent.month)
        print(lastMonthComponent.month)
        if todayComponent.month > lastMonthComponent.month {
             difference = 12 - todayComponent.month + lastMonthComponent.month
            print("DIFFERENCE IS \(difference)")
        } else {
         difference = lastMonthComponent.month - todayComponent.month
            print("DIFFERENCE IS last - today \(difference)")
        }
       
//        calculate how many months i miss
        let monthsToShow = allMonths.count - difference
        
        for i in 0 ..< monthsToShow{
            toDo.append(Date().MonthComponents(date: allMonths[i].month!).day!)
            done = challengesDoneThisMonth(i: i)
            today = Calendar.current.dateComponents(in: .current, from: Date()) as NSDateComponents
            month.append(Calendar.current.dateComponents(in: .current, from: allMonths[i].month!) as NSDateComponents)
            addButtonCell(i: i)
            addCircle(i:i)
            print("CIAO FRANCIII")
            print(i)
            addImage(i: i)
            addUIView(i: i, done: done, toDo: toDo[i] )
            addlabel(i: i, done: done, toDo: toDo[i] )
        }
    }
    func addButtonCell(i: Int) {
        let cell = UIButton()
        cell.frame = CGRect(x: 110, y: 20 + i*( 130 ), width: Int(view.frame.width) - 130, height: 130)
        scrollView.addSubview(cell)
//        if month[i].month > today.month {
//            cell.isEnabled = false
//               } else {
//            cell.isEnabled = true
//               }
        cell.tag = i
        cell.addTarget(self, action: #selector(goHistory), for: .touchUpInside)
    }
//    here we are adding a comment to can mae changes
    func addCircle(i: Int) {
        let border = UIButton()
        border.layer.borderWidth = 4
        if done == toDo[i]  {
            border.layer.borderColor = #colorLiteral(red: 0.4777091742, green: 0.4671983719, blue: 0.9636408687, alpha: 1)
             border.addTarget(self, action: #selector(showBadge), for: .touchUpInside)
        } else {
            border.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 0.2591235017)
        }
        border.tag = i
        border.layer.cornerRadius = 45
        border.backgroundColor = .none
        border.frame = CGRect(x: 20, y: 20 + 5 + i*( 130 ), width: 90, height: 90)
        scrollView.addSubview(border)
        
    }
    @objc func showBadge(sender: UIButton) {
        print("showBadge")
        print(sender.tag)
        print(allMonths[0].monthTitle!)
        let vc = PopUpBadge()
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        let j = Calendar.current.dateComponents(in: .current, from: Date()) as NSDateComponents
        vc.image = images[j.month - 1]!
        vc.label.text = allMonths[sender.tag].monthTitle
//       let vc =  self.storyboard?.instantiateViewController(withIdentifier: "showBadge") as? PopUpBadge
        
        self.present(vc, animated: true, completion: nil)
        
    }

    @objc func goHistory(sender: UIButton) {
        item = 0
        for i in 0 ..< sender.tag {
            item += toDo[i]
        }
        print(month[sender.tag].month)
        if month[sender.tag].month == today.month {
            item += today.day
        } else {
             item += toDo[sender.tag]
        }
//        print("\(item)")
        performSegue(withIdentifier: "historyFromAchievment", sender: self)
    }
    
    func addImage(i: Int){
        let imageView = UIImageView()
        imageView.frame = CGRect(x: 25, y: 20 + 10 + i*( 130 ), width: 80, height: 80)
        var image = UIImage()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 40

        if done == toDo[i] {
            let j = Calendar.current.dateComponents(in: .current, from: Date()) as NSDateComponents
            image = images[j.month - 1]!
        } else {
            image = UIImage(data: allMonths[i].monthImage!)!
        }
        imageView.image = image
        scrollView.addSubview(imageView)
    }
    
    func addlabel(i: Int, done: Int, toDo: Int){
        let label = MyLabel(labelTitle: "\(allMonths[i].monthTitle!)", lines: 2, labelFont: .custom)
        label.frame = CGRect(x: 125, y: 20 + i*( 130 ), width: Int(view.frame.width) - 145, height: 50)
        scrollView.addSubview(label)
        let label1 = MyLabel(labelTitle: "\(formatter.string(from: allMonths[i].month!)) \(done)/\(toDo)", labelFont: .regular)
        
        label1.frame = CGRect(x: 125, y: 20 + 50 + i*( 130 ), width: Int(view.frame.width) - 145, height: 20)
        scrollView.addSubview(label1)
    }
    
    func addUIView(i: Int, done: Int, toDo: Int){
        let backView = UIView()
        
//        if month[i].month > today.month {
//            setView(myView: backView, color: #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 0.2591235017))
//
//        } else {
            setView(myView: backView, color: #colorLiteral(red: 0.4777091742, green: 0.4671983719, blue: 0.9636408687, alpha: 0.2619327911))
//        }
        backView.frame = CGRect(x: 125, y: 20 + 73 + i*( 130 ), width: Int(view.frame.width) - 145, height: 17)
        
        let frontView = UIView()
        setView(myView: frontView, color: #colorLiteral(red: 0.4777091742, green: 0.4671983719, blue: 0.9636408687, alpha: 1))
        frontView.frame =  CGRect(x: 125, y: 20 + 73 + i*( 130 ), width:  Int(backView.frame.width)*done/toDo, height: 17)
        
    }
    func setView(myView: UIView,color: CGColor){
        myView.layer.cornerRadius = 9
        myView.layer.backgroundColor = color
        scrollView.addSubview(myView)
    }
    
    func challengesDoneThisMonth(i : Int) -> Int{
        print("siamo in \(i)")
        var counter = 0
        let month = Date().MonthComponents(date: allMonths[i].month!)
        for challenge in allChallenges {
            let day = Date().MonthComponents(date: challenge.day!)
            print(month.year)
            print(day.year)
            if  (month.year,month.month, true) == (day.year,day.month, challenge.done){
                counter += 1
            }
        }
        return counter
    }
    // MARK: - Navigation

          // In a storyboard-based application, you will often want to do a little preparation before navigation
          override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
              if segue.identifier == "historyFromAchievment" {
                  let VC = segue.destination as! HistoryVC
                  VC.item = item
                  print(VC.item)
              }
      }
}
// MARK: - Extension Date

extension Date {
    
    func MonthComponents(date: Date) -> DateComponents {
        let components:NSDateComponents = Calendar.current.dateComponents(in: .current, from: date) as NSDateComponents
        components.month += 1
        components.day = 1
        components.day -= 1
        let last = Calendar.current.date(from: components as DateComponents)!
        return Calendar.current.dateComponents(in: .current, from: last)
    }
    
    func missingDays() -> Int  {
        var counterDays = 0
        let components:NSDateComponents = Calendar.current.dateComponents([.day], from: self) as NSDateComponents
        let endDay = MonthComponents(date: Date()).day
        while (components.day < endDay!){
            components.day += 1
            counterDays += 1
        }
        return counterDays
    }
}
