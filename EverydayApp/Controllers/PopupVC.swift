//
//  PopupVC.swift
//  EverydayApp
//
//  Created by Simone Formisano on 13/06/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import UIKit

class PopupVC: UIViewController {
    
    @IBOutlet weak var rewardsBtn: UIButton!
    @IBOutlet weak var congratsLabel: UILabel!
    @IBOutlet weak var monthChallengePic: UIImageView!
    @IBOutlet weak var monthChallengeName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        rewardsBtn.layer.cornerRadius = rewardsBtn.bounds.height / 2
        let months = CoreDataController.shared.loadAllMonths()
        let monthChallenge = months.filter({Date.extractMonth(date: $0.month!) == Date.extractMonth(date: Date())}).first
        
        monthChallengeName.text = monthChallenge!.monthTitle
        
        monthChallengePic.image = UIImage(data: (monthChallenge?.monthImage)!)
        monthChallengePic.layer.cornerRadius = monthChallengePic.bounds.width / 2
        monthChallengePic.clipsToBounds = true
        
        let userDefault = UserDefaults.standard
        let username = userDefault.string(forKey: "username")
        congratsLabel.text = "Congratulations \(username!)! \n You have completed your daily challenges for the month and earned this reward. Look back on your achievements and come back tomorrow for more fun!"
    }
}
