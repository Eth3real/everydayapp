//
//  OnboardingPartThreeVC.swift
//  EverydayApp
//
//  Created by Giada Ciotola on 16/05/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//


import UIKit

class OnboardingPartThree: UIViewController, UITextFieldDelegate {

//    VAR buttons First Question
// 
//    @IBOutlet var firstQuestionButtons: [UIButton]!
//    @IBOutlet var secondQuestionButtons: [UIButton]!
//    @IBOutlet var thirdQuestionButtons: [UIButton]!
    
    
    @IBOutlet weak var usernameTextField: UITextField!
    
    @IBOutlet weak var startButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        usernameTextField.delegate = self
        setupKeyboard()
        designStartButton()
//        roundMarginButtons()
        
    }

    func designStartButton() {

        startButton.layer.cornerRadius = startButton.bounds.height / 2
        startButton.alpha = 1
        startButton.clipsToBounds = true

    }
    
    private func setupKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        if usernameTextField.text!.isEmpty {
        startButton.isEnabled = false
        } else {
            startButton.isEnabled = true
//            print(CoreDataController.shared.loadAllMonths())
//                if CoreDataController.shared.loadAllMonths().count <= 0 {
//                print("adding May challenges")
//                addMay()
//            }
        }
    }
    //    this function dismiss the keyboard by clicking the send button on it
        func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {
            self.view.endEditing(true)
            if usernameTextField.text!.isEmpty {
                   startButton.isEnabled = false
                   } else {
                       startButton.isEnabled = true
//                       print(CoreDataController.shared.loadAllMonths())
//                           if CoreDataController.shared.loadAllMonths().count <= 0 {
//                           print("adding May challenges")
//                           addMay()
//                       }
                   }
            return true
        }
    
          
      @objc func dismissKeyboard() {
          view.endEditing(true)
        if usernameTextField.text!.isEmpty {
        startButton.isEnabled = false
        } else {
            startButton.isEnabled = true
//            print(CoreDataController.shared.loadAllMonths())
//                  if CoreDataController.shared.loadAllMonths().count <= 0 {
//                  print("adding May challenges")
//                  addMay()
//              }
              }
      }
    
    
//    func roundMarginButtons() {
//        let buttonBarList: [[UIButton]] = [firstQuestionButtons, secondQuestionButtons, thirdQuestionButtons]
//        buttonBarList.forEach { (buttonBar) in
//            let firstBtn = buttonBar.first
//            firstBtn!.clipsToBounds = true
//            firstBtn!.layer.cornerRadius = firstBtn!.bounds.height / 2
//            firstBtn!.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
//
//            let lastBtn = buttonBar.last
//              lastBtn!.clipsToBounds = true
//              lastBtn!.layer.cornerRadius = lastBtn!.bounds.height / 2
//              lastBtn!.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
//        }
//    }
    
    
//    //    FIRST QUESTION BUTTONS
//
//    @IBAction func firstQuestionButtonPressed(_ sender: UIButton) {
//
//                let tag = sender.tag
//                for button in firstQuestionButtons {
//                    if button.tag <= tag {
//        //                selected
//                        button.setImage(UIImage(named: "buttonOnBoardingActive"), for: .normal)
//                    } else {
//        //                not selected
//                        button.setImage(UIImage(named: "buttonOnBoardingInactive"), for: .normal)
//
//                    }
//                }
//
//    }
    
//    //    SECOND QUESTION BUTTONS
//
//    @IBAction func secondQuestionButtonPressed(_ sender: UIButton) {
//
//              let tag = sender.tag
//              for button in secondQuestionButtons {
//                  if button.tag <= tag {
//        //                selected
//                      button.setImage(UIImage(named: "buttonOnBoardingActive"), for: .normal)
//                  } else {
//        //                not selected
//                      button.setImage(UIImage(named: "buttonOnBoardingInactive"), for: .normal)
//
//                    }
//                }
//    }
    
    
    
//       //    THIRD QUESTION BUTTONS
//
//    @IBAction func thirdQuestionButtonPressed(_ sender: UIButton) {
//
//              let tag = sender.tag
//              for button in thirdQuestionButtons {
//                  if button.tag <= tag {
//        //                selected
//                      button.setImage(UIImage(named: "buttonOnBoardingActive"), for: .normal)
//                 } else {
//        //                not selected
//                      button.setImage(UIImage(named: "buttonOnBoardingInactive"), for: .normal)
//
//                    }
//                }
//    }
    
    
      @IBAction func startButtonPressed(_ sender: Any) {
      
          let userDefaultStore = UserDefaults.standard
          userDefaultStore.set(usernameTextField.text, forKey: "nickname")
          self.performSegue(withIdentifier: "pass to Main", sender: self)
        
      }
      
      
      override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          if (segue.identifier  == "pass to Main") {
            let destinationNavigationController = segue.destination as! UINavigationController
            let destinationDailyVC = destinationNavigationController.topViewController as! DailyTaskVC
          destinationDailyVC.passedUsernameString = usernameTextField.text!
      }
    }

}
