//
//  PageControlViewController.swift
//  EverydayApp
//
//  Created by Giada Ciotola on 17/05/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import UIKit
import CoreData

class PageControlViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {

    lazy var orderedViewControllers: [UIViewController] = {
        return [self.newViewController(viewController: "ScreenOne"),
                self.newViewController(viewController: "ScreenTwo"),
                self.newViewController(viewController: "ScreenThree")]
    }()
    
    var pageControl = UIPageControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        self.dataSource = self
        
        
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
        
        configurePageControl()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Onboarding only once workaround
        if UserDefaults.standard.string(forKey: "nickname") != nil {
            self.orderedViewControllers[0].view.alpha = 0.0
            performSegue(withIdentifier: "goHome", sender: nil)
        }
        // ------------
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        self.pageControl.currentPage = orderedViewControllers.firstIndex(of: pageContentViewController)!
    }
    
    func configurePageControl() {
        pageControl = UIPageControl(frame: CGRect(x: 0, y: UIScreen.main.bounds.maxY - 30, width: UIScreen.main.bounds.width, height: 10))
        pageControl.numberOfPages = orderedViewControllers.count
        pageControl.currentPage = 0
        pageControl.tintColor = .black
        pageControl.pageIndicatorTintColor = .lightGray
        pageControl.currentPageIndicatorTintColor = .black
//        pageControl.currentPageIndicatorTintColor = UIColor(red: 179/255, green: 172/255, blue: 250/255, alpha: 1)
        self.view.addSubview(pageControl)
    }
    
    func newViewController(viewController: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: viewController)
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
                   return nil
               }
               
               let nextIndex = viewControllerIndex + 1
               guard orderedViewControllers.count > nextIndex else {
                   return nil
               }
               
               return orderedViewControllers[nextIndex]
    }
    
}
