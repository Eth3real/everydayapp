//
//  CheckChallenges.swift
//  EverydayApp
//
//  Created by FrancescaCiancio on 12/06/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import Foundation
import UIKit
import CoreData

func checkChallengesToAdd() {
    var todayComponent:NSDateComponents!
    var lastDayComponent:NSDateComponents?
    var oldChallenges = CoreDataController.shared.loadAllChallenges()
    var totalPreviousChallenges = oldChallenges.count
    print("\(oldChallenges.count ) total c precedenti")
    todayComponent = Calendar.current.dateComponents(in: .current, from: Date()) as NSDateComponents
    if totalPreviousChallenges != 0 {
        lastDayComponent = Calendar.current.dateComponents(in: .current, from: (oldChallenges.last?.day)!) as NSDateComponents
    }
    var month: Int!
    for i in 1...12 {
        month = i
        print("month is \(month!)")
        if totalPreviousChallenges == 0 && todayComponent.month == month {
            addNewChallenges(whichMonth: month, totalPreviousChallenges: totalPreviousChallenges)
            print("first month")
        } else if totalPreviousChallenges != 0 {
            print(totalPreviousChallenges)
            if todayComponent.year == lastDayComponent!.year {
                if todayComponent.month > month - 1 && lastDayComponent!.month < month {
                    print("missing month \(month!)")
                    addNewChallenges(whichMonth: month, totalPreviousChallenges: totalPreviousChallenges)
                    
                }
            } else  if todayComponent.year > lastDayComponent!.year {
                print(todayComponent.year)
                //                if lastDayComponent!.month < month {
                if lastDayComponent!.month > month {
                    print("different year")
                    addNewChallenges(whichMonth: month, totalPreviousChallenges: totalPreviousChallenges)
                    
                }
            }
        }
        print("update challenges count")
        oldChallenges = CoreDataController.shared.loadAllChallenges()
        totalPreviousChallenges = oldChallenges.count
        if totalPreviousChallenges != 0 {
            lastDayComponent = Calendar.current.dateComponents(in: .current, from: (oldChallenges.last?.day)!) as NSDateComponents
        }
        
    }
}


func addNewChallenges(whichMonth: Int, totalPreviousChallenges: Int) {
    var changes = true
    switch whichMonth {
    case 1: addJanuary()
    case 2: let elements = Calendar.current.dateComponents(in: .current, from: (Date())) as NSDateComponents
        addFebruary(elements: elements.day )
    case 3: addMarch()
    case 4: addApril()
    case 5: addMay()
    case 6: addJune()
    case 7: addJuly()
    case 8: addAugust()
    case 9: addSeptember()
    case 10: addOctober()
    case 11: addNovember()
    case 12: addDecember()
    default:
        print("no new challenges")
        changes = false
    }
    if changes {
        print("change numeration month \(whichMonth)")
        var myCounter = 1
        let challenges = CoreDataController.shared.loadAllChallenges()
        
        for i in totalPreviousChallenges...challenges.count-1 {
            let components:NSDateComponents = Calendar.current.dateComponents(in: .current, from: challenges[i].day!) as NSDateComponents
            components.day = myCounter
            components.month = whichMonth
            myCounter += 1
            let last = Calendar.current.date(from: components as DateComponents)!
            challenges[i].day = last
        }
        
        CoreDataController.shared.saveChanges()
        print("\(whichMonth) with the right numeration")
        let challengesNow =  CoreDataController.shared.loadAllChallenges()
        for challenge in challengesNow {
            print("\(challenge.day!)\n\(challenge.challenge!)")
        }
    }
    print("end month \(whichMonth)")
}
