//
//  CoreDataController.swift
//  EverydayApp
//
//  Created by FrancescaCiancio on 11/05/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import UIKit
import CoreData

class CoreDataController {
    //     to be directly used in the viewControllers
    static let shared = CoreDataController()
    private var context: NSManagedObjectContext
    
    init() {
        let application = UIApplication.shared.delegate as! AppDelegate
        self.context = application.persistentContainer.viewContext
    }
    //    this function fetch all the data
    func loadAllChallenges() -> [Entity]{
        var allChallenges : [Entity] = []
        let fetchRequest: NSFetchRequest<Entity> = Entity.fetchRequest()
        
        do {
            let challenges = try self.context.fetch(fetchRequest)
            
            if challenges.count > 0 {
                allChallenges = challenges
                print("Data fetched")
            }else {
                print("There are no data to fetch")
                
            }
            
            
        } catch let errore {
            print(" Error fetching data: \n \(errore) \n")
        }
        return allChallenges
    }
    //    this function save the changes in Entity
    func saveChanges() {
        if context.hasChanges {
            do {
                try self.context.save()
            } catch let errore {
                print("Changes didn't be save")
                print(" Error: \n \(errore) \n")
            }
        }
        print("Changes saved")
        
    }
    func clearChallenges() {
        for challenge in loadAllChallenges() {
            challenge.done = false
            challenge.whenDone = .none
        }
        saveChanges()
    }
    
    func deleteChallenge(challenge: Entity){
        context.delete(challenge)
        saveChanges()
    }
    
    func deleteMonth(month: Months){
        context.delete(month)
        saveChanges()
    }
    //    this function add a new element to the entity
    func addChallenge(challenge: String, notion: String, challengeType: String, resultImage: Data){
        let entity = NSEntityDescription.entity(forEntityName: "Entity", in: self.context)
        let newLChallenge = Entity(entity: entity!, insertInto: self.context)
        newLChallenge.done = false
        newLChallenge.challenge = challenge
        newLChallenge.notion = notion
        newLChallenge.day = Date()
        newLChallenge.challengeType = challengeType
        newLChallenge.resultImage = Data()
        
        saveChanges()
    }
    
    func loadAllMonths() -> [Months]{
        print("fetch months")
        var allMonths : [Months] = []
        let fetchRequest: NSFetchRequest<Months> = Months.fetchRequest()
        
        do {
            let months = try self.context.fetch(fetchRequest)
            
            if months.count > 0 {
                allMonths = months
            }else {
                print("There are no months")
                
            }
            
            
        } catch let errore {
            print("Fetch didn't work")
            print(" Error: \n \(errore) \n")
        }
        return allMonths
    }
    
    func addMonth(month: Date, monthTitle: String, image: UIImage){
        let entity = NSEntityDescription.entity(forEntityName: "Months", in: self.context)
        let newMonth = Months(entity: entity!, insertInto: self.context)
        newMonth.month = month
        newMonth.monthTitle = monthTitle
        //I need to convert image to data to can save it with coredata
        //        let data = image.jpegData(compressionQuality: 8.0)
        let data = image.pngData()
        newMonth.monthImage = data
        
        if context.hasChanges {
            do {
                try self.context.save()
            } catch let errore {
                print("New month didn't be save")
                print(" Error: \n \(errore) \n")
            }
        }
        print("Add month")
        
    }
    
    
    func changePhoto(image: UIImage) {
        
        let fetchRequest: NSFetchRequest<TheSettings> = TheSettings.fetchRequest()
        
        do {
            let pi = try self.context.fetch(fetchRequest)
            
            if pi.count > 0 {
                pi.first?.profileImage = image.jpegData(compressionQuality: 8.0)
            } else {
                print("There is no image saved")
                let entity = NSEntityDescription.entity(forEntityName: "TheSettings", in: self.context)
                let profileImage = TheSettings(entity: entity!, insertInto: self.context)
                
                //         I need to convert image to data to can save it with coredata
                let data = image.jpegData(compressionQuality: 8.0)
                profileImage.profileImage = data
                
            }
            
            
        } catch let errore {
            print("Fetch didn't work")
            print(" Error: \n \(errore) \n")
        }
        
        saveChanges()
    }
    
    
    func takePhotoSaved() -> UIImage {
        
        var imageSaved = TheSettings()
        let fetchRequest: NSFetchRequest<TheSettings> = TheSettings.fetchRequest()
        
        do {
            
            let pi = try self.context.fetch(fetchRequest)
            
            if pi.count > 0 {
                imageSaved = pi.first!
            } else {
                print("There is no image saved")
                
                return UIImage()
                
                
            }
            
            
        } catch let errore {
            print("Fetch didn't work")
            print(" Error: \n \(errore) \n")
        }
        return UIImage(data: imageSaved.profileImage!)!
    }
    
    
    /// Function to load only the challenge of the day. Used il dailyVC
    func getTodaysChallenge() -> Entity {
        let challenges = loadAllChallenges()
        let todaysChallenge = challenges.filter({Date.getDay(date: $0.day!) == Date.getTodaysDay() && Date.extractMonth(date: $0.day!) == Date.extractMonth(date: Date())}).first
        return todaysChallenge!
    }
    
}
