//
//  Entity+CoreDataClass.swift
//  EverydayApp
//
//  Created by FrancescaCiancio on 11/05/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Entity)
public class Entity: NSManagedObject {

}
