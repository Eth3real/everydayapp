//
//  AllMonths.swift
//  EverydayApp
//
//  Created by FrancescaCiancio on 02/06/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import Foundation
import UIKit
import CoreData
func addMonths(){
    var addMonth = false
    let previousMonth = CoreDataController.shared.loadAllMonths().count
    print("I have \(previousMonth) months")
    print(CoreDataController.shared.loadAllChallenges().count)
    if previousMonth/12 == 0 {
        //        if i have no months or if an year is been passed
        let todayComponent: NSDateComponents = Calendar.current.dateComponents(in: .current, from: Date()) as NSDateComponents
        //        let firtMonth  = todayComponent.month - 6
        var allMonths = CoreDataController.shared.loadAllMonths()
        var firstMonth = 6
        if previousMonth != 0 {
            let lastMonth: NSDateComponents = Calendar.current.dateComponents(in: .current, from: (allMonths.last?.month)!) as NSDateComponents
            let firstDayComponent: NSDateComponents = Calendar.current.dateComponents(in: .current, from: (allMonths.first?.month)!) as NSDateComponents
            let allChallenges = CoreDataController.shared.loadAllChallenges()
            let lastDay: NSDateComponents = Calendar.current.dateComponents(in: .current, from: (allChallenges.last?.day)!) as NSDateComponents
            lastDay.month += 1
            let last = Calendar.current.date(from: lastDay as DateComponents)!
            let nextLast: NSDateComponents = Calendar.current.dateComponents(in: .current, from: (last)) as NSDateComponents
            if (lastDay.month,nextLast.month) == (lastMonth.month,todayComponent.month){
                print("sono nel ciclo per aggiungere un nuovo anno")
                print(nextLast.month)
                print(allChallenges.count)
                
                addMonth = true
            }
            //  i have first month ( the same month i have to add now)
            firstMonth  = firstDayComponent.month
            //        if todayComponent.month != lastMonth.month + 1 {
            ////            addMonth = true
            //        }
        } else {
            print("ho 0 mesi, quindi inizio anno")
            //            i do not have previous months so my first month is the month of today
            firstMonth = todayComponent.month
            addMonth = true
        }
        
        if addMonth {
            print("first month is \(firstMonth)")
            if firstMonth > 5 && firstMonth < 12{
                print("adding the title of 6-11")
                //                if i do not have previou month and the month of today is not between june and november i do not add
                //                if i have previous months and the month i have to add is between june and november
                //        june
                CoreDataController.shared.addMonth(month: Date(), monthTitle: "Do one thing you wouldn't usually do", image: UIImage(named: "oneThing")!)
                //        july
                CoreDataController.shared.addMonth(month: Date(), monthTitle: "Learn a cool trick a day",image: UIImage(named: "trick")!)
                //        august
                CoreDataController.shared.addMonth(month: Date(), monthTitle: "Cook a new recipe everyday",image: UIImage(named: "cook")!)
                //        september
                CoreDataController.shared.addMonth(month: Date(), monthTitle: "User Challenges",image: UIImage(named: "user")!)
                //        october
                CoreDataController.shared.addMonth(month: Date(), monthTitle: "Live your life in a minimalistic way",image: UIImage(named: "minimalism")!)
                //        november
                CoreDataController.shared.addMonth(month: Date(), monthTitle: "Only use your non-dominant hand",image: UIImage(named: "nonDomain")!)
                allMonths = CoreDataController.shared.loadAllMonths()
                
                for i in 0...(11 - firstMonth){
                    print(i)
                    CoreDataController.shared.deleteMonth(month: allMonths[i])
                    print("delete title month \(i)")
                }
                
                var monthCounter = todayComponent.month
                allMonths = CoreDataController.shared.loadAllMonths()
                var newYear = false
                for i in previousMonth...allMonths.count-1 {
                    print("numerating the months just added")
                    let components:NSDateComponents = Calendar.current.dateComponents(in: .current, from: allMonths[i].month!) as NSDateComponents
                    if components.month == 12 {
                        newYear = true
                    }
                    components.month = monthCounter
                    monthCounter += 1
                    if newYear{
                        components.year += 1
                    }
                    let last = Calendar.current.date(from: components as DateComponents)!
                    allMonths[i].month = last
                }
                CoreDataController.shared.saveChanges()
                print("printing the months with the right numeration")
                let newMonths = CoreDataController.shared.loadAllMonths()
                for month in newMonths {
                    print("\(month.month!)")
                }
                addMissingMonths()
            } else {
                print("the month of today is between december to may")
                //                if the first month is between december to may
                addMissingMonths()
            }
            
        }
        
    }else {
        print("mfor who download the app previously")
        addMissingMonths()
    }
}
func addMissingMonths() {
    print("adding missing months")
    let previousMonths = CoreDataController.shared.loadAllMonths()
    //    if allMonths.count != 0 {
    //
    let lastMonth: NSDateComponents = Calendar.current.dateComponents(in: .current, from: (previousMonths.last?.month)!) as NSDateComponents
    let todayComponent: NSDateComponents = Calendar.current.dateComponents(in: .current, from: Date()) as NSDateComponents
    //    var firstMonth: NSDateComponents = todayComponent
    //    firstMonth = Calendar.current.dateComponents(in: .current, from: (allMonths.first?.month)!) as NSDateComponents
    if lastMonth.month == 11 || previousMonths.count == 0{
        
        
        
        //        december
        CoreDataController.shared.addMonth(month: Date(), monthTitle: "Do one thing you wouldn't usually do", image: UIImage(named: "oneThing")!)
        //        january
        CoreDataController.shared.addMonth(month: Date(), monthTitle: "Learn a cool trick a day",image: UIImage(named: "trick")!)
        //        fabruary
        CoreDataController.shared.addMonth(month: Date(), monthTitle: "Cook a new recipe everyday",image: UIImage(named: "cook")!)
        //        march
        CoreDataController.shared.addMonth(month: Date(), monthTitle: "User Challenges",image: UIImage(named: "user")!)
        //        april
        CoreDataController.shared.addMonth(month: Date(), monthTitle: "Live your life in a minimalistic way",image: UIImage(named: "minimalism")!)
        //        may
        CoreDataController.shared.addMonth(month: Date(), monthTitle: "Only use your non-dominant hand",image: UIImage(named: "nonDomain")!)
        //        june
        CoreDataController.shared.addMonth(month: Date(), monthTitle: "Do one thing you wouldn't usually do", image: UIImage(named: "oneThing")!)
        //        july
        CoreDataController.shared.addMonth(month: Date(), monthTitle: "Learn a cool trick a day",image: UIImage(named: "trick")!)
        //        august
        CoreDataController.shared.addMonth(month: Date(), monthTitle: "Cook a new recipe everyday",image: UIImage(named: "cook")!)
        //        september
        CoreDataController.shared.addMonth(month: Date(), monthTitle: "User Challenges",image: UIImage(named: "user")!)
        //        october
        CoreDataController.shared.addMonth(month: Date(), monthTitle: "Live your life in a minimalistic way",image: UIImage(named: "minimalism")!)
        //        november
        CoreDataController.shared.addMonth(month: Date(), monthTitle: "Only use your non-dominant hand",image: UIImage(named: "nonDomain")!)
        
        var allMonths = CoreDataController.shared.loadAllMonths()
        var monthCounter = 12
        print("ho i mesi:")
        print(allMonths.count)
        print(allMonths.count/12)
        let monthNumber: Int = allMonths.count-(12 * (allMonths.count/12))
        print("i have to delete the last \(monthNumber)")
        print(allMonths.count - monthNumber)
        for i in (allMonths.count - monthNumber)...allMonths.count - 1{
            CoreDataController.shared.deleteMonth(month: allMonths[i])
            print("delete month \(i)")
        }
        allMonths = CoreDataController.shared.loadAllMonths()
        for i in previousMonths.count...allMonths.count-1 {
            print(previousMonths.count)
            print(i)
            print("numerating the months just added")
            let components:NSDateComponents = Calendar.current.dateComponents(in: .current, from: allMonths[i].month!) as NSDateComponents
            components.month = monthCounter
            monthCounter += 1
            let last = Calendar.current.date(from: components as DateComponents)!
            allMonths[i].month = last
        }
        
        
        
        
        CoreDataController.shared.saveChanges()
        print("printing the months with the right numeration")
        let newMonths = CoreDataController.shared.loadAllMonths()
        for month in newMonths {
            print("\(month.month!)")
        }
        
    }
    
    //}
}
