//
//  December.swift
//  EverydayApp
//
//  Created by Jade Bowl on 19/09/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import Foundation
import UIKit
import CoreData

func addDecember() {
    //    one thing
    //    1
    CoreDataController.shared.addChallenge(challenge: "Choose today's clothes with your eyes closed", notion: "Orange shirt with purple pants? Why not! You may not look like a fashion icon, but you will definitely be unique!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    2
    CoreDataController.shared.addChallenge(challenge: "Open a package of biscuit and play biscuit Jenga", notion: "Why only eat it when you can also have fun with it?", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    3
    CoreDataController.shared.addChallenge(challenge: "Go shopping in your pyjamas ", notion: "When you do things that make you uncomfortable and push you out your comfort zone, that's when growth happens!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    4
    CoreDataController.shared.addChallenge(challenge: "Learn the numbers in Finnish", notion: "One of the first things you learn in a language are often numbers. So why not start by learning to count from 1 to 10?", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    5
    CoreDataController.shared.addChallenge(challenge: "Take a cold shower", notion: "It's not so bad once your body gets used to the temperature! And it can be incredibly beneficial to your physical and mental health.", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    6
    CoreDataController.shared.addChallenge(challenge: "Make an origami", notion: "A fun way to improve your concentration and make beautiful art!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    7
    CoreDataController.shared.addChallenge(challenge: "Wear a shirt inside out", notion: "Silly? Sure! But a great way to start random conversations.", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    8
    CoreDataController.shared.addChallenge(challenge: "Make a small donation to a cause you support", notion: "A small contribution can make a difference and helping others can be empowering and make you feel happier and more fulfilled.", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    9
    CoreDataController.shared.addChallenge(challenge: "Text someone you haven't seen in years", notion: "Everybody loves to hear from old friends! Start with \"Hey I haven't talked to you in a while, how are you?\"", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    10
    CoreDataController.shared.addChallenge(challenge: "Send a random selfie to a random friend with no explanation", notion: "While you're having fun with new experiences why not bring your friends into it too?", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    11
    CoreDataController.shared.addChallenge(challenge: "Today say yes to any proposal you get", notion: "Unless they are illegal or dangerous, of course!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    12
    CoreDataController.shared.addChallenge(challenge: "Live a day on a budget", notion: "Don't spend any money today! You can cook food you already have or make yourself a cup of coffee instead of going to the bar.", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    13
    CoreDataController.shared.addChallenge(challenge: "Wear two different coloured socks", notion: "Just because they don't naturally go together doesn't mean they can't be friends.So stand out and break the social norms!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    14
    CoreDataController.shared.addChallenge(challenge: "Read a poem", notion: "It can trigger memories and emotions and help you develop empathy.", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    15
    CoreDataController.shared.addChallenge(challenge: "Buy yourself something silly", notion: "Treat yourself! But with only 2€/$, being it for decoration or without a purpose.", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    16
    CoreDataController.shared.addChallenge(challenge: "Write a poem", notion: "Poetry helps you appreciate the world around you. So, especially if you've never done it before, give it a try!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    17
    CoreDataController.shared.addChallenge(challenge: "Learn itsy bitsy spider on the piano", notion: "Don't have a piano? Your phone can work too and it's one of the easiest songs out there!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    18
    CoreDataController.shared.addChallenge(challenge: "Read something from a writer you disagree with", notion: "Being open to other's opinions is not always easy. Try to find some useful thoughts even if it's said by someone you don't agree with.", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    19
    CoreDataController.shared.addChallenge(challenge: "Describe your day in three words", notion: "Sometimes it's healthy to reflect on your day. Take few minutes to think about what happen today and how it made you feel.", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    20
    CoreDataController.shared.addChallenge(challenge: "Jump rope twenty times", notion: "It's super fun and it improves your coordination. Don't have a jumping rope? Find a cord and watch out for the lamp!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    21
    CoreDataController.shared.addChallenge(challenge: "Subscribe to an online course you want to take", notion: "Learning online is very convenient and finding the time to add new skills to your CV, or grow as a person, can be so beneficial!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    22
    CoreDataController.shared.addChallenge(challenge: "Make a caricature of someone you care about", notion: "They are a fun way to exaggerate the most prominent traits of a person and help you really notice the people that surround you!", challengeType: ChallengeTypes.drawing, resultImage: Data())
    //    23
    CoreDataController.shared.addChallenge(challenge: "Plan a trip for the next holidays", notion: "Traveling has been proven to be very beneficial for our brain health. Where have you always wanted to go?", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    24
    CoreDataController.shared.addChallenge(challenge: "Make a popsicle", notion: "All you need is a popsicle mold (can be as easy as toothpicks in an ice cube tray), some juice, sugar and water!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    25
    CoreDataController.shared.addChallenge(challenge: "Eat dinner in a different spot at the table", notion: "Even silly new experiences like this one can help new neurons survive, thrive and create new connections. Give it a go!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    26
    CoreDataController.shared.addChallenge(challenge: "Change the order of the objects on your desk", notion: "Switching things up can activate your brain in different ways, by having to relearn where everything is again.", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    27
    CoreDataController.shared.addChallenge(challenge: "Write a letter to your best friend", notion: "Writing a physical letter with pen and paper allows you to really think about what you'd like to communicate to the other person.", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    28
    CoreDataController.shared.addChallenge(challenge: "Ask a friend to go for a walk", notion: "It's always nice to get some movement enjoying the company of someone you like. And don't forget \"pics or it didn't happen\"!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    29
    CoreDataController.shared.addChallenge(challenge: "Take a selfie with a stranger", notion: "It might sound a bit crazy, but it can actually boost your confidence!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    30
    CoreDataController.shared.addChallenge(challenge: "Buy someone a flower", notion: "A simple act of kindness can strengthen a friendship or make a social connection, but most importantly, you could make someone's day!", challengeType: ChallengeTypes.picture, resultImage: Data())
//    31
    CoreDataController.shared.addChallenge(challenge: "Read a comic book", notion: "Let's finish this month with a smile", challengeType: ChallengeTypes.picture, resultImage: Data())
    print("december added")
}




