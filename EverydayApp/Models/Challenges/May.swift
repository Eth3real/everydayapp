//
//  May.swift
//  EverydayApp
//
//  Created by Jade Bowl on 19/09/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import Foundation
import UIKit
import CoreData
func addMay() {
    //    user challenges
    //    1
    CoreDataController.shared.addChallenge(challenge: "Draw a shape", notion: "Drawing increases your motor skills and your eye-hand-brain coordination, but doing it with your non-dominant can also help you think in a different way!", challengeType: ChallengeTypes.drawing, resultImage: Data())
       //    2
       CoreDataController.shared.addChallenge(challenge: "Write your name 5 times", notion: "Might be harder than you think, it will be like going back to learning how to write.", challengeType: ChallengeTypes.picture, resultImage: Data())
       //    3 se abbiamo challenge type deve essere video
       CoreDataController.shared.addChallenge(challenge: "Brush your teeth", notion: "A simple gesture, but that can help you train your non-dominant hand while creating new connections between neurons.", challengeType: ChallengeTypes.picture, resultImage: Data())
       //    4
       CoreDataController.shared.addChallenge(challenge: " Eat lunch", notion: "Switch fork and knife and start eating with your non-dominant hand.", challengeType: ChallengeTypes.picture, resultImage: Data())
       //    5
    CoreDataController.shared.addChallenge(challenge: "Use the mouse", notion: "Might be quite uncomfortable at first, but with a bit of practice you'll master it!", challengeType: ChallengeTypes.picture, resultImage: Data())
       //    6 video
        CoreDataController.shared.addChallenge(challenge: "Record a video", notion: " Your non-dominant hand might not be as strong and steady.Try to record a video in horizontal for 30 seconds.", challengeType: ChallengeTypes.picture, resultImage: Data())
       //    7 video
       CoreDataController.shared.addChallenge(challenge: " Cut a piece of paper", notion: "Use your dominant hand to hold it and your non-dominant hand to cut it, but always be careful!", challengeType: ChallengeTypes.picture, resultImage: Data())
       
       //    8 video
       CoreDataController.shared.addChallenge(challenge: "Send a text", notion: "Try to send a text with your non-dominant hand. Can you manage to spell everything right?", challengeType: ChallengeTypes.picture, resultImage: Data())
       //    9 video
       CoreDataController.shared.addChallenge(challenge: "Dust your room", notion: " Kill to birds with one stone: clean you room and train your non-dominant hand!", challengeType: ChallengeTypes.picture, resultImage: Data())
       
       //    10
       CoreDataController.shared.addChallenge(challenge: "Brush your hair", notion: "Training your non-dominant hand for small tasks can improve your self-control.", challengeType: ChallengeTypes.picture, resultImage: Data())
       
       //    11 video
       CoreDataController.shared.addChallenge(challenge: "Pour yourself a glass of water", notion: "It's not only a question of strength but also coordination. Try not to pour any water outside the glass.", challengeType: ChallengeTypes.picture, resultImage: Data())
       //    12
       CoreDataController.shared.addChallenge(challenge: "Make a sandwich.", notion: "Picking up the slices of bread, spreading mayo, putting on ham: do it all with your non-dominant hand!.", challengeType: ChallengeTypes.picture, resultImage: Data())
       
       //    13 video
       CoreDataController.shared.addChallenge(challenge: "Scroll your feed and leave a comment", notion: " No matter which social as long as you use your non-dominant hand!", challengeType: ChallengeTypes.picture, resultImage: Data())
       
       //    14
       CoreDataController.shared.addChallenge(challenge: "Take a selfie", notion: "Strike a pose and take a selfie. Does it look different when you're holding it with your non-dominant hand?", challengeType: ChallengeTypes.picture, resultImage: Data())
       //    15 da qui sono responsabile
       CoreDataController.shared.addChallenge(challenge: "Draw a tree", notion: "Keep improving motor skills and your eye-hand-brain coordination with your non-dominant hand.", challengeType: ChallengeTypes.picture, resultImage: Data())
       
       //    16
    CoreDataController.shared.addChallenge(challenge: "Use computer", notion: "Start with \"Hello word!\" and then write something funny", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    17
        CoreDataController.shared.addChallenge(challenge: "Wash dishes", notion: "It will be funny and train you if you don’t brake any dish or glass. Be carefull!.", challengeType: ChallengeTypes.picture, resultImage: Data())
        
        //    18
        CoreDataController.shared.addChallenge(challenge: "Shopping", notion: "Pull the shopping cart with your non-domain hand ", challengeType: ChallengeTypes.picture, resultImage: Data())
        
        //    19 vedi se trovi di meglio
        CoreDataController.shared.addChallenge(challenge: "Wear makeup or shorten your beard ", notion: "Train until your work is enough fine to go out!", challengeType: ChallengeTypes.picture, resultImage: Data())
        
        //    20
        CoreDataController.shared.addChallenge(challenge: " Drink coffee", notion: "If you don’t drink coffee take your favourite drink!", challengeType: ChallengeTypes.picture, resultImage: Data())
        
        //    21
        CoreDataController.shared.addChallenge(challenge: "Fold a t-shirt", notion: "Only with one hand, only with non-domain ones", challengeType: ChallengeTypes.picture, resultImage: Data())
        
        //    22
        CoreDataController.shared.addChallenge(challenge: " Tighten a screw ", notion: "Find all the loose screws ", challengeType: ChallengeTypes.drawing, resultImage: Data())
        
        //    23
        CoreDataController.shared.addChallenge(challenge: "Wash the ground ", notion: "It is time to take care of the home!", challengeType: ChallengeTypes.picture, resultImage: Data())
        
        //    24
        CoreDataController.shared.addChallenge(challenge: "Play Jenga", notion: "Suggest your friend to use too only non-domaind hand. It will be more funny!", challengeType: ChallengeTypes.picture, resultImage: Data())
        
        //    25
        CoreDataController.shared.addChallenge(challenge: "Use the key", notion: "Be sure to have locked well your home with your non-domain hand.", challengeType: ChallengeTypes.picture, resultImage: Data())
        //    26
        CoreDataController.shared.addChallenge(challenge: "Give a five", notion: "Congratulate to someone with your non-domain hand!", challengeType: ChallengeTypes.picture, resultImage: Data())
        
        //    27
        CoreDataController.shared.addChallenge(challenge: "Paint a gate", notion: "If you don’t know the colour, choose white, it is easier to use an other colour if you don’t like it.", challengeType: ChallengeTypes.picture, resultImage: Data())
        
        //    28
        CoreDataController.shared.addChallenge(challenge: "Use your wallet", notion: "Use the pocket you usually don’t use, it will help you to use your non-domain hand. ", challengeType: ChallengeTypes.picture, resultImage: Data())
        
        //    29
        CoreDataController.shared.addChallenge(challenge: "Cut a chicken cutlet", notion: "Don’t you like chicken? Use red meat!", challengeType: ChallengeTypes.picture, resultImage: Data())
        //    30
        CoreDataController.shared.addChallenge(challenge: "Draw yourself", notion: "Compare your masterpiece with the non-dominant hand with that of the dominant hand", challengeType: ChallengeTypes.picture, resultImage: Data())
//    31
    CoreDataController.shared.addChallenge(challenge: "Take off the dust", notion: "Your home will be more clean", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    print("may added")
}

