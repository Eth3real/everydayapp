//
//  October.swift
//  EverydayApp
//
//  Created by Jade Bowl on 19/09/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//


import Foundation
import UIKit
import CoreData
func addOctober() {
    //    user challenges
    //    1
    CoreDataController.shared.addChallenge(challenge: "Count how many t-shirt you own", notion: "How many t-shirt are in your closet? Do you really need all of them?", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    2
    CoreDataController.shared.addChallenge(challenge: "Organise your bookshelf and donate a book", notion: "Once you organised your bookshelf pick a book you'd like to donate and go hide it somewhere in the city for someone else could find it. Remember to leave a note!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    3 se abbiamo challenge type deve essere video
    CoreDataController.shared.addChallenge(challenge: "Live a day zero waste", notion: "Did you know that there are people that live their lives without producine any waste? Try to do the same for yourself and for the planet!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    4
    CoreDataController.shared.addChallenge(challenge: " Describe yourself in 3 words", notion: " Take the time to think about how you would describe yourself. Finding them can be also useful for interviews.", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    5 video describing
    CoreDataController.shared.addChallenge(challenge: " Take 15 minutes to meditate", notion: " Meditation has a lot of benefits such as reducing stress, helping with anxiety and develop focus and concentration.", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    6
    CoreDataController.shared.addChallenge(challenge: " Delete the apps you are not using anymore", notion: " Our phones are an important part of out lives. Taking the time to declutter them from all the unused apps can be beneficial for our mind state as well.", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    7 video
    CoreDataController.shared.addChallenge(challenge: " Take the day off social media", notion: " As addicting as they are, social media are often cause of anxiety and low self-esteem. Try to live a day without it and see how it feels.", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    8
    CoreDataController.shared.addChallenge(challenge: " Throw away 3 objects you don't use anymore ", notion: " Our houses are filled with objects we barely use or even see. Take the time to find them and throw 3 of them away.", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    9 video
    CoreDataController.shared.addChallenge(challenge: " Quit a bad habit", notion: " We all have vices we'd like to quit. For today try to give up on one of them, being it too much coffee, smoking or biting your nails.", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    10
    CoreDataController.shared.addChallenge(challenge: " Buy a plant", notion: " It will bring life and energy to your house, while purifying the air around. But remember to take care of it.", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    11
    CoreDataController.shared.addChallenge(challenge: " Clean a drawer", notion: " We all have a drawer full of cra... stuff, with all the things we were not able to throw away. Pick one and clean it up.", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    12
    CoreDataController.shared.addChallenge(challenge: " Take the time to do something you love.", notion: " Having a busy schedule is often what makes us give up on our hobbies. Try to find some time for one of them.", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    13
    CoreDataController.shared.addChallenge(challenge: " Dress only with the 3 primary colours", notion: " Today only wear clothes that are either red, blue or yellow!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    14
    CoreDataController.shared.addChallenge(challenge: " Fold your clothes like a pro", notion: " By learning how to fold your clothes properly you will save so much space in your drawers but also in your suitcase!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    15
    CoreDataController.shared.addChallenge(challenge: " Use it or get rid of it", notion: " Find an object that you don't use anymore, and then decide if either use it or give it away.", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    16
    CoreDataController.shared.addChallenge(challenge: " Organise your Desktop", notion: " If the first thing you see when turning on your computer is a messy Desktop, now it's the time to clean it up!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    17
    CoreDataController.shared.addChallenge(challenge: " Delete a chat", notion: " Our phones are submerged by chats archived there that you'll never read again. Delete one of them.", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    18
    CoreDataController.shared.addChallenge(challenge: " Organise the shopping for the whole week", notion: " Being organised can help you save a lot of time. Decide what you are going to cook for the whole week!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    19
    CoreDataController.shared.addChallenge(challenge: " Wear a solid colour shirt", notion: " Minimalism is often associated with simplicity, so for today wear a solid colour shirt, no prints, no nothing.", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    20
    CoreDataController.shared.addChallenge(challenge: " Organise your socks drawer", notion: " Today organise your socks by length and colour!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    21
    CoreDataController.shared.addChallenge(challenge: " Cook with only 3 ingredients", notion: " You can sure add oil and salt, but try to only use 3 ingredients for today's meal!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    22
    CoreDataController.shared.addChallenge(challenge: " Do yoga", notion: " With yoga you can improve your respiration and muscle strength but it's also fundamental for improving mental calmness.", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    23
    CoreDataController.shared.addChallenge(challenge: " Don't waste paper", notion: " Throughout the day we often waste too much paper. Today make an effort to avoid that.", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    24
    CoreDataController.shared.addChallenge(challenge: " Colour a mandala", notion: " It's incredibly relaxing and the end result so beautiful! Either print it or colour it from your phone.", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    25
    CoreDataController.shared.addChallenge(challenge: " Go see a panorama", notion: " Go out and look for a panorama! Just hang there, remember to breathing and enjoy the view.", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    26
    CoreDataController.shared.addChallenge(challenge: " Only pay with your card or phone", notion: " Today avoid bringing with you coins or cash! Your pocket will fill much lighter.", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    27
    CoreDataController.shared.addChallenge(challenge: " Clean the kitchen", notion: " Today is all about cleaning! Wash the dishes left in the sink, swipe off crumbs and put everything in place.", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    28
    
    CoreDataController.shared.addChallenge(challenge: " Trim your wallet", notion: " Sometimes our wallets get filled with business cards, photos, receipt and more! Try to clean it up a bit.", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    29
    CoreDataController.shared.addChallenge(challenge: " Unsubscribe to emails", notion: " We subscribe to so many emails that we don't even read half the time. Go to your inbox and unsubscribe to them.", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    30
    CoreDataController.shared.addChallenge(challenge: " Make a to-do list for the day", notion: " Sometimes is very useful to make a to-do list of the things we have to take care for the day. Grab your phone or pen and paper.", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    31
    CoreDataController.shared.addChallenge(challenge: " Book a massage", notion: " Have you always wanted one but never had the chance to book one? Now it's the time! Book yourself a relaxing massage.", challengeType: ChallengeTypes.picture, resultImage: Data())
    print("october added")
    
}
