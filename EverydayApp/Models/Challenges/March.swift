//
//  March.swift
//  EverydayApp
//
//  Created by Jade Bowl on 19/09/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import Foundation
import UIKit
import CoreData
func addMarch() {
    //    user challenges
    //    1
    CoreDataController.shared.addChallenge(challenge: "Sing over a song you like", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    2
    CoreDataController.shared.addChallenge(challenge: "Draw your superhero", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    3
    CoreDataController.shared.addChallenge(challenge: "Learn how to make a bird call", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    4
    CoreDataController.shared.addChallenge(challenge: "Speaker for a day for radio Sterna", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    5
    CoreDataController.shared.addChallenge(challenge: "Share a photo on social media", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    6
    CoreDataController.shared.addChallenge(challenge: "Buy a newspaper", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    7
    CoreDataController.shared.addChallenge(challenge: "Paint an old pair of shoes", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    8
    CoreDataController.shared.addChallenge(challenge: "Read the books of a saga you've only seen the movies of", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    9
    CoreDataController.shared.addChallenge(challenge: "Introduce two friends that have the same initial", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    10
    CoreDataController.shared.addChallenge(challenge: "Take a timelapse of your favourite place", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    11
    CoreDataController.shared.addChallenge(challenge: "Make a live portrait of someone", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    12
    CoreDataController.shared.addChallenge(challenge: "Go on the escalators but in opposite direction", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    13
    CoreDataController.shared.addChallenge(challenge: "Pick a random episode of a TV Series", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    14
    CoreDataController.shared.addChallenge(challenge: "Buy funny socks", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    15
    CoreDataController.shared.addChallenge(challenge: "Clean your bathroom", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    16
    CoreDataController.shared.addChallenge(challenge: "Go to karaoke", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    17
    CoreDataController.shared.addChallenge(challenge: "Add a new song to your playlist", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    18
    CoreDataController.shared.addChallenge(challenge: "Start watching a movie saga", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    19
    CoreDataController.shared.addChallenge(challenge: "Put a piece of furniture in a room it doesn't belong", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    20
    CoreDataController.shared.addChallenge(challenge: "Go to the scariest ride of an amusement park", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    21
    CoreDataController.shared.addChallenge(challenge: "Download a new app", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    22
    CoreDataController.shared.addChallenge(challenge: "Make a particular tea infusion", notion: "", challengeType: ChallengeTypes.drawing, resultImage: Data())
    
    //    23
    CoreDataController.shared.addChallenge(challenge: "Play pool online with strangers", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    24
    CoreDataController.shared.addChallenge(challenge: "Book a concert", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    25
    CoreDataController.shared.addChallenge(challenge: "Watch an ASMR video to relax", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    26
    CoreDataController.shared.addChallenge(challenge: "Change your hairstyle", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    27
    CoreDataController.shared.addChallenge(challenge: "Draw a new logo for Sterna", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    28
    CoreDataController.shared.addChallenge(challenge: "Learn how to code on Scratch", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    29
    CoreDataController.shared.addChallenge(challenge: "Buy a present to someone you care about", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    30
    CoreDataController.shared.addChallenge(challenge: "Play with an old console", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
//    31
    CoreDataController.shared.addChallenge(challenge: "Play ping pong", notion: "", challengeType: ChallengeTypes.picture, resultImage: Data())
    print("march added")
}

