//
//  August.swift
//  EverydayApp
//
//  Created by FrancescaCiancio on 12/06/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import Foundation
import UIKit
import CoreData
func addAugust() {
    //    cook
    //    1
    CoreDataController.shared.addChallenge(challenge: "Ever mixed pancetta with cabbage?", notion: "You never know what incredible recipe you could find!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    2
    CoreDataController.shared.addChallenge(challenge: "Make a dessert with no sugar", notion: "There are tons of sugar-free deserts out there, like cheesecake or cookies. Pick your favourite and start baking.", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    3
    CoreDataController.shared.addChallenge(challenge: "Eat breakfast for dinner and vice versa", notion: "Always fun to switch it up a little bit!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    4
    CoreDataController.shared.addChallenge(challenge: "Make a fancy frittata", notion: "Add your favourite ingredients to it and make the perfect frittata! Remember to cook it on low heat for starters.", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    5
    CoreDataController.shared.addChallenge(challenge: "Be vegetarian for one day", notion: "Try to only cook and eat vegetarian foods for the whole day!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    6
    CoreDataController.shared.addChallenge(challenge: "Melon, tomato and prosciutto", notion: "ATry all those ingredients together and make a wonderful salad!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    7
    CoreDataController.shared.addChallenge(challenge: "Ever tried to cook pasta with walnuts?", notion: "You can also add some cheese, and basil or parsley to make it even more tasty!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    8
    CoreDataController.shared.addChallenge(challenge: "Make a vegan pie", notion: "Especially if you have vegan friends it could be very cool to learn how to bake something nice for them!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    9
    CoreDataController.shared.addChallenge(challenge: "Pizza night", notion: "Ever tried tuna and onion on your pizza? And what about olives and tomatoes? Let your imagination run free!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    10
    CoreDataController.shared.addChallenge(challenge: "Make a piadina", notion: "The most famous Italian flatbread sandwich! Fill it with cheese, ham, salad, and whatever you like.", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    11
    CoreDataController.shared.addChallenge(challenge: "Choose ingredients from your fridge with your eyes closed", notion: "The start to cook a meal with whatever you got!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    12
    CoreDataController.shared.addChallenge(challenge: "Cook like it's 120.000 b.C.", notion: "So with no fire lol", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    13
    CoreDataController.shared.addChallenge(challenge: "Recreate a dish you used to eat as a child", notion: "Surely we all have a favourite food we loved as children, try to remember what it was and make it!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    14
    CoreDataController.shared.addChallenge(challenge: "Discover a new country through food", notion: "Pick a country that has a different and rich food culture from yours and try to recreate one of their dishes.", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    15
    CoreDataController.shared.addChallenge(challenge: "Make finger food", notion: "Best recipes for parties: from pesto pastries to tomato bruschette, from tarts to roll-ups!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    16
    CoreDataController.shared.addChallenge(challenge: "Beer Can Chicken", notion: "Did you know that in this recipe you have to cook the chicken.. on a can?!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    17
    CoreDataController.shared.addChallenge(challenge: "Salt Crust Sea Bream", notion: "The salt is there to protect the fish from overcooking, but the fish won't get too salty at all!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    18
    CoreDataController.shared.addChallenge(challenge: "Avocado pasta", notion: "Mix the avocado with basil, garlic and lemon juice, then add salt and pepper and make a very creamy \"sauce\"!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    19
    CoreDataController.shared.addChallenge(challenge: "Super digestable pepperoni", notion: "After peeling them, cut them into strips and put them in a frying pan with oil. Add sugar and salt as required!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    20
    CoreDataController.shared.addChallenge(challenge: "Pasta with yogurt and zucchini", notion: "Both zucchini and yogurt are rich in potassium, so give it a try for this very light and healthy dish!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    21
    CoreDataController.shared.addChallenge(challenge: "Make bean falafel", notion: "Directly from Egypt a very tasty and inviting recipe! Give it a go.", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    22
    CoreDataController.shared.addChallenge(challenge: "Make a milkshake", notion: "All you need is ice-cream, milk and some fruit. Blend them all together to make a perfect summer dessert!", challengeType: ChallengeTypes.drawing, resultImage: Data())
    
    //    23
    CoreDataController.shared.addChallenge(challenge: "Make some crepes", notion: "One of the most loved dessert, but you could also make it savoury!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    24
    CoreDataController.shared.addChallenge(challenge: "Cook without salt", notion: "YFor today instead of using salt, try different spices to get the same delicious result!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    25
    CoreDataController.shared.addChallenge(challenge: "Keto bread", notion: "A very healthy alternative to actual bread. Go low-carb for the day!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    26
    CoreDataController.shared.addChallenge(challenge: "Instant ice cream", notion: "Mix milk, sugar and vanilla in one plastic bag. Add ice and salt in another bag around the first. Now shake it for 15 minutes!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    27
    CoreDataController.shared.addChallenge(challenge: "Savoury muffins", notion: "You can make them with brie, courgette and red pepper or even with triple cheese and onion. Get creative!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    28
    CoreDataController.shared.addChallenge(challenge: "Make eggplant \"meatballs\"", notion: "What better substitute for meat than eggplant? You can make them fried or with sauce, still quite delish!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    29
    CoreDataController.shared.addChallenge(challenge: "Make the typical meal of where you're from", notion: "A fun way to rediscover your origins or just learn how to make something traditional of where you come from!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    30
    CoreDataController.shared.addChallenge(challenge: "Make pumpkin flowers in batter", notion: "You'll need flour, some water, oil and salt. Fry them on a pan and enjoy this very tasty meal!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    31
    CoreDataController.shared.addChallenge(challenge: "Learn how to make lasagne", notion: "One of the most beloved dishes but not the easiest. Little tip: go with a proper Italian recipe!", challengeType: ChallengeTypes.picture, resultImage: Data())
    print("august added")
}
