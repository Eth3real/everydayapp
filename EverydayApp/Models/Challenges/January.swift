//
//  January.swift
//  EverydayApp
//
//  Created by Jade Bowl on 19/09/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//


import Foundation
import UIKit
import CoreData
func addJanuary(){
    //    cool trick
    //    1
    CoreDataController.shared.addChallenge(challenge: "Learn the cup song", notion: "All you need is a cup, the rest is history. ", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    2
    CoreDataController.shared.addChallenge(challenge: "Open a bottle with a piece of paper", notion: "Such a cool party trick!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    3
    CoreDataController.shared.addChallenge(challenge: "Rubik's cube", notion: "Solving the famous Rubik's Cube is easier than you think, can you find the trick?", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    4
    CoreDataController.shared.addChallenge(challenge: "Juggling-time", notion: "Start with only 3 balls and try to switch them in your hands. Practice makes you better!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    5
    CoreDataController.shared.addChallenge(challenge: "Create a penguin out of tinfoil", notion: "Cause, why not?", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    6
    CoreDataController.shared.addChallenge(challenge: "Make your own soap bubbles", notion: "All you need is soap for dishes, water and a straw!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    7
    CoreDataController.shared.addChallenge(challenge: "Split an apple with bare hands", notion: "Get rid of the stem, grab the apple with both hands and start applying some pressure on the right spots!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    8
    CoreDataController.shared.addChallenge(challenge: "Whistle with candy wrapper", notion: "Whistle with candy wrapper", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    9
    CoreDataController.shared.addChallenge(challenge: "Be.. be.. beatbox", notion: "There is no instrument. You are the instrument! Give it a go starting from the basics, and see what comes out!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    10
    CoreDataController.shared.addChallenge(challenge: "Sign language alphabet", notion: "There is a sign language for every language. Try to learn the sign language alphabet of your mother tongue.", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    11
    CoreDataController.shared.addChallenge(challenge: "Cut an onion without crying", notion: "There are many ways to do this, like using a sharp knife or putting the onion under water while cutting it.", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    12
    CoreDataController.shared.addChallenge(challenge: "Learn how to do calligraphy", notion: "There are plenty of benefits for your mental health linked to the practice of calligraphy, for example reducing stress.", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    13
    CoreDataController.shared.addChallenge(challenge: "Roll a coin across your knuckles", notion: "A very cool trick that you could show off pretty much anywhere! Just gotta have some money.", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    14
    CoreDataController.shared.addChallenge(challenge: "Fold your clothes like a pro", notion: "By learning how to fold your clothes properly you will save so much space in your drawers but also in your suitcase!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    15
    CoreDataController.shared.addChallenge(challenge: "Learn how to play chess", notion: "TThe benefits are countless, from increasing both sides of the brain to improving your memory, concentration and problem-solving skills!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    16
    CoreDataController.shared.addChallenge(challenge: "Say the alphabet backwards", notion: "Could be struggling at the beginning but it's a very good way to stimulate your brain. Give it a try!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    17
    CoreDataController.shared.addChallenge(challenge: "Proper cereal box folding", notion: "Maybe you thought you were doing this right, but type \"cereal box folding\" on Google and learn how wrong you were!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    18
    CoreDataController.shared.addChallenge(challenge: "How to tie a tie", notion: "Very important and helpful skill to have, either for a work interview or to help your dad or partner looking fly!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    19
    CoreDataController.shared.addChallenge(challenge: "Open a jar with a rubber band", notion: "Very cool kitchen hack to easily open a very tight jar!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    20
    CoreDataController.shared.addChallenge(challenge: "How to make balloon animals", notion: "Start by learning how to make the dog, which is the easiest one but then don't hold back, make a whole farm!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    21
    CoreDataController.shared.addChallenge(challenge: "Knit a scarf", notion: "Knitting is used for relaxation, relieving stress and creativity. So why not learning how to make yourself a beautiful scarf!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    22
    CoreDataController.shared.addChallenge(challenge: "Make a drink", notion: "It's cheap and can make you the star of the party. Why not trying an easy one but then add something more?", challengeType: ChallengeTypes.drawing, resultImage: Data())
    
    //    23
    CoreDataController.shared.addChallenge(challenge: "Magic trick", notion: "The amount of cool magic tricks out there is unbelievable. Try to learn one too, maybe one with a deck of cards?", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    24
    CoreDataController.shared.addChallenge(challenge: "Make a paper boat", notion: "You'll be surprised on how many people will be impressed by that!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    25
    CoreDataController.shared.addChallenge(challenge: "Pen spinning", notion: "Before starting to learn this super cool trick, exercise your hand a little bit.", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    26
    CoreDataController.shared.addChallenge(challenge: "Napkin rose", notion: "Next time you have guests over surprise them with this beautiful skill! Learn how to fold your napkins in the most beautiful ways!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    27
    CoreDataController.shared.addChallenge(challenge: "Instantly turn water into ice", notion: "All you need is a water bottle kept in the freezer for around 2 hours, a glass and ice cubes and the trick is done!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    28
    CoreDataController.shared.addChallenge(challenge: "Draw an animal with your hand", notion: "If you have kids or little cousins and nephews this trick it's pretty cool!", challengeType: ChallengeTypes.picture, resultImage: Data())
    
    //    29
    CoreDataController.shared.addChallenge(challenge: "Make a recorder out of a carrot", notion: "Probably you never even thought of this, but how awesome would that be?", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    30
    CoreDataController.shared.addChallenge(challenge: "Replicate a cool photography creative trick", notion: "There are so many tricks out there to make unbelievably awesome photographs. Just found one and give it a go!", challengeType: ChallengeTypes.picture, resultImage: Data())
    //    31
    CoreDataController.shared.addChallenge(challenge: "Learn a magic trick", notion: "Everybody loves magic! Learn how to make something disappear or.. pull a rabbit out of a hat?", challengeType: ChallengeTypes.picture, resultImage: Data())
    print("january added")
}
