//
//  Entity+CoreDataProperties.swift
//  EverydayApp
//
//  Created by FrancescaCiancio on 11/05/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//
//

import Foundation
import CoreData
import UIKit


extension Entity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Entity> {
        return NSFetchRequest<Entity>(entityName: "Entity")
    }
    
    // The image (drawing, picture) that stay saved after the challenge completation
    
    @NSManaged public var resultImage: Data? // The image is in formart of binary data
    @NSManaged public var challengeType: String?
    @NSManaged public var challenge: String?
    @NSManaged public var notion: String?
    @NSManaged public var done: Bool
    @NSManaged public var day: Date?

}
