//
//  Months+CoreDataProperties.swift
//  EverydayApp
//
//  Created by FrancescaCiancio on 16/05/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//
//

import Foundation
import CoreData


extension Months {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Months> {
        return NSFetchRequest<Months>(entityName: "Months")
    }

    @NSManaged public var month: Date?
    @NSManaged public var monthTitle: String?

}
