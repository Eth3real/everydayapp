//
//  Settings+CoreDataProperties.swift
//  EverydayApp
//
//  Created by Giada Ciotola on 11/06/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//
//

import Foundation
import CoreData


extension TheSettings {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TheSettings> {
        return NSFetchRequest<TheSettings>(entityName: "TheSettings")
    }

    @NSManaged public var profileImage: Data?

}
