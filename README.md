<br />
<div align="center">
  <a href="https://gitlab.com/Eth3real/everydayapp">
    <img src="https://i.ibb.co/PghVK53/Icon-2x.png" alt="Icon-2x" alt="App Icon" width="150" height="150" style="border-radius:16%">
  </a>

  <h3 align="center">Sterna - One Challenge A Day</h3>



  <div align="center">
    <strong>Sterna</strong> was developed for all the people that want to make <br />
    the most of their lives.
    <br />
    <br />
    
  </div>
</div>


## Table of Contents

* [About the Project](#about-the-project)
* [App Store](#app-store)
* [Team](#the-team)
* [Contacts](#contacts)


## About The Project
<div align="center">
<img src="https://i.ibb.co/jZfgNzz/Immaginepromozionale-def-5.png" alt="Immaginepromozionale-def-5" alt="Promotional Image">
</div>
<br />
<br />
With Sterna you can push yourself every day through creative and interesting challenges, and begin to make the most of your life! <br />
The <strong>main goal</strong> is to help people experience something new every day for one year, in order to change their mindset and the way <br />
they welcome new opportunities.
<br /> 
Every day you will get a different challenge, which is part of a more wide monthly challenge. When you complete a whole month <br />
you will get rewarded with a beautiful <strong>illustrated badge</strong>!



## App Store

<a href="https://apps.apple.com/us/app/id1513837581?fbclid=IwAR2ijSbRs0U2feo6-Mbh56rfzvEtwX4UTNzuxADvjI5XKtDZAgSngsgSlt4">
    <img src="https://i.ibb.co/S6rd0PM/1024px-App-Store-mac-OS-svg.png" alt="App Store" width="150" height="150">
</a>
<br />
<br />
It's currently possible to download our app on the <i><a href="https://apps.apple.com/us/app/id1513837581?fbclid=IwAR2ijSbRs0U2feo6-Mbh56rfzvEtwX4UTNzuxADvjI5XKtDZAgSngsgSlt4">App Store</a></i>. Every <i>feedback</i> is very appreciated.
<br />
<br />

## The Team

<strong>Project Manager:</strong>

- Giada Ciotola

 <strong>Developers:</strong>
- Simone Formisano
- Francesca Ciancio
- Giada Ciotola

 <strong>UI/UX Designers:</strong>
 
- Carmine Acierno
- Luca Vespoli

<strong>Business:</strong>
- Antonio Alfonso
- Vincenzo Coppola


## Contacts

- Giada Ciotola - <a href="https://www.linkedin.com/in/giadaciotola/"><img src="https://is3-ssl.mzstatic.com/image/thumb/Purple123/v4/83/3f/a9/833fa9b8-2b53-9115-30ee-a5394b9a5026/AppIcon-0-0-1x_U007emarketing-0-0-0-6-0-0-sRGB-0-0-0-GLES2_U002c0-512MB-85-220-0-0.png/246x0w.png" alt="Logo" width="15" height="15" style="border-radius:16%"></a>

- Francesca Ciancio - <a href="https://www.linkedin.com/in/francesca-ciancio-dev/"><img src="https://is3-ssl.mzstatic.com/image/thumb/Purple123/v4/83/3f/a9/833fa9b8-2b53-9115-30ee-a5394b9a5026/AppIcon-0-0-1x_U007emarketing-0-0-0-6-0-0-sRGB-0-0-0-GLES2_U002c0-512MB-85-220-0-0.png/246x0w.png" alt="Logo" width="15" height="15" style="border-radius:16%"></a>

- Simone Formisano - <a href="https://www.linkedin.com/in/simoneformisano/"><img src="https://is3-ssl.mzstatic.com/image/thumb/Purple123/v4/83/3f/a9/833fa9b8-2b53-9115-30ee-a5394b9a5026/AppIcon-0-0-1x_U007emarketing-0-0-0-6-0-0-sRGB-0-0-0-GLES2_U002c0-512MB-85-220-0-0.png/246x0w.png" alt="Logo" width="15" height="15" style="border-radius:16%"></a>

- Carmine Acierno - <a href="https://www.linkedin.com/in/carmine-acierno-221226188/"><img src="https://is3-ssl.mzstatic.com/image/thumb/Purple123/v4/83/3f/a9/833fa9b8-2b53-9115-30ee-a5394b9a5026/AppIcon-0-0-1x_U007emarketing-0-0-0-6-0-0-sRGB-0-0-0-GLES2_U002c0-512MB-85-220-0-0.png/246x0w.png" alt="Logo" width="15" height="15" style="border-radius:16%"></a>

- Antonio Alfonso - <a href="https://www.linkedin.com/in/iamantonioalfonso/"><img src="https://is3-ssl.mzstatic.com/image/thumb/Purple123/v4/83/3f/a9/833fa9b8-2b53-9115-30ee-a5394b9a5026/AppIcon-0-0-1x_U007emarketing-0-0-0-6-0-0-sRGB-0-0-0-GLES2_U002c0-512MB-85-220-0-0.png/246x0w.png" alt="Logo" width="15" height="15" style="border-radius:16%"></a>

- Vincenzo Coppola - <a href="https://www.linkedin.com/in/vincenzocoppola94/"><img src="https://is3-ssl.mzstatic.com/image/thumb/Purple123/v4/83/3f/a9/833fa9b8-2b53-9115-30ee-a5394b9a5026/AppIcon-0-0-1x_U007emarketing-0-0-0-6-0-0-sRGB-0-0-0-GLES2_U002c0-512MB-85-220-0-0.png/246x0w.png" alt="Logo" width="15" height="15" style="border-radius:16%"></a>
